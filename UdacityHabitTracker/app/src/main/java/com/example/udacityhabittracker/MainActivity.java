package com.example.udacityhabittracker;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.udacityhabittracker.data.HabitContract.HabitEntry;
import com.example.udacityhabittracker.data.HabitDbHelper;

public class MainActivity extends AppCompatActivity {
    private HabitDbHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDbHelper = new HabitDbHelper(this);
    }


    private void displayDatabaseInfo() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                HabitEntry._ID,
                HabitEntry.COLUMN_NAME,
                HabitEntry.COLUMN_DURATION,
                HabitEntry.COLUMN_DESC,
                HabitEntry.COLUMN_DANGER};

        Cursor cursor = db.query(HabitEntry.TABLE_NAME, projection, null, null, null, null, null);
        try {
            TextView displayView = (TextView) findViewById(R.id.text_view_info);
            displayView.setText(getString(R.string.instrucs) + "\n" +
                    getString(R.string.numDbRows) + ": " + cursor.getCount());
            displayView.append("\n\n" +
                    HabitEntry._ID + " - " +
                    HabitEntry.COLUMN_NAME + " - " +
                    HabitEntry.COLUMN_DURATION + " - " +
                    HabitEntry.COLUMN_DESC + " - " +
                    HabitEntry.COLUMN_DANGER + "\n\n"
            );

            int idColumnIndex = cursor.getColumnIndex(HabitEntry._ID);
            int nameColumnIndex = cursor.getColumnIndex(HabitEntry.COLUMN_NAME);
            int durationColumnIndex = cursor.getColumnIndex(HabitEntry.COLUMN_DURATION);
            int descColumnIndex = cursor.getColumnIndex(HabitEntry.COLUMN_DESC);
            int dangerColumnIndex = cursor.getColumnIndex(HabitEntry.COLUMN_DANGER);

            while(cursor.moveToNext()){
                int currentId = cursor.getInt(idColumnIndex);
                if(currentId < 11){
                    String currentName = cursor.getString(nameColumnIndex);
                    String currentDuration = cursor.getString(durationColumnIndex);
                    String currentDesc = cursor.getString(descColumnIndex);
                    int currentDanger = cursor.getInt(dangerColumnIndex);

                    displayView.append(("\n"
                            + currentId + " - "
                            + currentName + " - "
                            + currentDuration + " - "
                            + currentDesc + " - "
                            + currentDanger
                    ));
                }else{
                    break;
                }
            }
            displayView.append(("\n" + getString(R.string.maxResultsDisplayed)));
        } finally {
            cursor.close();
        }
    }
    
    private void insertDummyHabit(){
        HabitDbHelper pdbH = new HabitDbHelper(this);
        SQLiteDatabase db = pdbH.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(HabitEntry.COLUMN_NAME, getString(R.string.dummyName));
        values.put(HabitEntry.COLUMN_DURATION, getString(R.string.dummyDuration));
        values.put(HabitEntry.COLUMN_DESC, getString(R.string.dummyDesc));
        values.put(HabitEntry.COLUMN_DANGER, getResources().getInteger(R.integer.dummyDanger));

        long newRowId = db.insert(HabitEntry.TABLE_NAME, null, values);

        displayDatabaseInfo();
    }

    public void insertDummyHabitData(View view){
        insertDummyHabit();
    }

    @Override
    protected void onStart(){
        super.onStart();
        displayDatabaseInfo();
    }
    
}
