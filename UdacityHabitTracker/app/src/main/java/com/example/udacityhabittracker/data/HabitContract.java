package com.example.udacityhabittracker.data;

import android.provider.BaseColumns;

public final class HabitContract {

    private HabitContract() {}

    public static class HabitEntry implements BaseColumns {

        public static final String TABLE_NAME = "habit";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DURATION = "duration";
        public static final String COLUMN_DESC = "desc";
        public static final String COLUMN_DANGER = "danger";

        public static final int DANGER_UNKNOWN = 0;
        public static final int DANGER_LOW = 1;
        public static final int DANGER_MED = 2;
        public static final int DANGER_HIGH = 3;

    }

}
