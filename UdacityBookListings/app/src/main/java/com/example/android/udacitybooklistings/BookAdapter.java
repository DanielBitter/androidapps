package com.example.android.udacitybooklistings;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Dan on 2/25/2017.
 * https://www.sitepoint.com/custom-data-layouts-with-your-own-android-arrayadapter/
 */

public class BookAdapter extends ArrayAdapter<Book> {
    private final Context context;
    private final List<Book> bookData;
    private final int maxNameLength;

    public BookAdapter(Context ct, List<Book> eqs){
        super(ct, 0, eqs);
        this.context = ct;
        this.bookData = eqs;
        this.maxNameLength = context.getResources().getInteger(R.integer.lastNameMaxLength);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        String title = bookData.get(position).getTitle();
        String[] authors = bookData.get(position).getAuthor();
        int urlId = bookData.get(position).getUrlIconId();

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_result_item, null);
        }

        ImageView http = (ImageView) convertView.findViewById(R.id.imageViewUrlStatusIndicator);
        TextView titleTv = (TextView) convertView.findViewById(R.id.textViewResultTitle);
        TextView authorTv = (TextView) convertView.findViewById(R.id.textViewResultAuthor);

        http.setBackgroundResource(urlId);
        titleTv.setText(title);
        String firstAuthor = authors[0];
        String elipses = context.getResources().getString(R.string.authorElipses);

        if(     firstAuthor.length() > this.maxNameLength) {
            if (firstAuthor.contains(" ")) {
                StringBuilder shortenedAuthor = new StringBuilder();
                String[] names = firstAuthor.split(" ");

                shortenedAuthor.append(String.valueOf(names[0].charAt(0)))
                        .append(context.getResources().getString(R.string.authorSeparator))
                        .append(names[names.length - 1]);

                firstAuthor = shortenedAuthor.toString();

            } else {
                firstAuthor = firstAuthor.substring(0, this.maxNameLength - 1);
                firstAuthor.concat(elipses);
            }
        }
        if(authors.length > 1){
            firstAuthor.concat(elipses);
        }

        authorTv.setText(firstAuthor);
        return convertView;
    }
}