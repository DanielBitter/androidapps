package com.example.android.udacitybooklistings;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper methods related to requesting and receiving book data
 */
public final class QueryUtils {
    private static final String LOG_TAG = "QueryUtils";

    public QueryUtils() {
    }

    private static List<Book> extractFeatureFromJson(String jsonData) {

        List<Book> books = new ArrayList<>();

        try {
            if (jsonData == null || TextUtils.isEmpty(jsonData) || jsonData.length() < 1) {
                return null;
            }

            JSONObject root = new JSONObject(jsonData);
            JSONArray rootFeaturesArray = root.getJSONArray("items");

            for (int i=0;i<rootFeaturesArray.length();i++) {
                JSONObject bookIdx = rootFeaturesArray.getJSONObject(i);
                JSONObject bkProperties = bookIdx.getJSONObject("volumeInfo");

                String title = bkProperties.getString("title");

                JSONArray authorArray = bkProperties.getJSONArray("authors");
                int numAuthors = authorArray.length();
                String[] authors = new String[numAuthors];
                for(int j=0;j<numAuthors;j++){
                    authors[j] = String.valueOf(authorArray.get(j));
                }

                String publisher = bkProperties.getString("publisher");
                int pages = bkProperties.getInt("pageCount");
                String languageCode = bkProperties.getString("language");
                String infoUrl = String.valueOf(bkProperties.get("canonicalVolumeLink"));

                String webReader = "";
                JSONObject accessInfo = bookIdx.getJSONObject("accessInfo");
                JSONObject pdfInfo = accessInfo.getJSONObject("pdf");
                Boolean pdfAvail = pdfInfo.getBoolean("isAvailable");
                if(pdfAvail){
                    webReader = String.valueOf(accessInfo.get("webReaderLink"));
                }

                Book bk = new Book();
                bk.setTitle(title);
                bk.setAuthor(authors);
                bk.setPublisher(publisher);
                bk.setNumPages(pages);
                bk.setLanguage(languageCode);
                bk.setInfoUrl(infoUrl);
                bk.setWebReader(webReader);

                books.add(bk);
                Log.v(LOG_TAG,"Added book: "+title);
            }

        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the JSON results", e);
        }

        return books;
    }

    public static List<Book> fetchBookData(String requestUrl) {
//        try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();} //show loading indicator

        Log.v(LOG_TAG,"fetchBookData()");
        // Create URL object
        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making the HTTP request.", e);
            return null;
        }

        // Extract relevant fields from the JSON response and create a list of {@link Earthquake}s
        List<Book> books = extractFeatureFromJson(jsonResponse);

        // Return the list of {@link Earthquake}s
        return books;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        // If the URL is null, then return early.
        if (url == null || url.toString().length() < 1) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }


}