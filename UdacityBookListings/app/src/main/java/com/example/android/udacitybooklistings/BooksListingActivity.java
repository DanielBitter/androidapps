/**
 * https://www.caveofprogramming.com/guest-posts/custom-listview-with-imageview-and-textview-in-android.html
 * http://stackoverflow.com/questions/1805518/replacing-all-non-alphanumeric-characters-with-empty-strings
 */
package com.example.android.udacitybooklistings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class BooksListingActivity extends AppCompatActivity
{
    private String searchParam = "";
    private String inputError = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_listing);
        inputError = this.getString(R.string.errorValidInput);
        searchParam = getString(R.string.intentSearchParamKey);
    }

    public void startLoaderProcess(View view){
        doSearch();
    }

    public void doSearch(){
        Intent intent = new Intent(this, ResultsActivity.class);
        EditText input = (EditText) findViewById(R.id.editTextInputBox);
        String inputSearchTerm = input.getText().toString();
        inputSearchTerm = formatSearch(inputSearchTerm);
        if(searchParam != null || !searchParam.isEmpty()){
            intent.putExtra(searchParam, inputSearchTerm);
            startActivity(intent);
        }else{
            Toast.makeText(this, inputError, Toast.LENGTH_SHORT).show();
        }
    }

    public String formatSearch(String input){
        input = input.replaceAll("[^A-Za-z0-9 ]", "");
        input = input.trim();
        input = input.replaceAll(" ", "%20");
        return input;
    }
}
