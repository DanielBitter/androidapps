package com.example.android.udacitybooklistings;


import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * Created by Dan on 3/21/2017.
 */

public class BookLoader
        extends AsyncTaskLoader<List<Book>> {
    private String url;

    public BookLoader(Context context, String urlArg) {
        super(context);
        this.url = urlArg;
    }

    @Override
    public List<Book> loadInBackground() {
        if(url == null || url.length() < 1){
            return null;
        }
        List<Book> bookData = QueryUtils.fetchBookData(url);
        return bookData;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
