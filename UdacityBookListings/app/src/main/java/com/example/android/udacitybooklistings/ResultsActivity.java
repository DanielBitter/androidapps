/**
 * https://www.caveofprogramming.com/guest-posts/custom-listview-with-imageview-and-textview-in-android.html
 */
package com.example.android.udacitybooklistings;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ResultsActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<Book>>
{
    public RelativeLayout layoutResults;

    private BookAdapter mAdapter;
    private TextView mEmptyStateTextView;
    private String intentParamSearchTerm = "";
    private static final String BOOKS_REQUEST_URL =
            "https://www.googleapis.com/books/v1/volumes?q=";
    private static final int BOOK_LOADER_ID = 1;
    private static String searchParam = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        searchParam = getApplicationContext().getString(R.string.intentSearchParamKey);

        Intent intent = getIntent();
        intentParamSearchTerm = intent.getStringExtra(searchParam);
        intentParamSearchTerm = BOOKS_REQUEST_URL.concat(intentParamSearchTerm);

        mEmptyStateTextView = (TextView) findViewById(R.id.textViewEmpty);
        ListView bookListView = (ListView) findViewById(R.id.resultsListView);
        bookListView.setEmptyView(mEmptyStateTextView);

        mAdapter = new BookAdapter(this, new ArrayList<Book>());

        bookListView.setAdapter(mAdapter);
        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String openingUrl = getApplicationContext().getString(R.string.urlOpening);
                String url = mAdapter.getItem(position).getWebReader();
                if(url.length() < 1){
                    openingUrl = getApplicationContext().getString(R.string.errorNoUrl);
                }
                Toast.makeText(getApplicationContext(), openingUrl, Toast.LENGTH_SHORT).show();

                Uri webpage = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        LoaderManager loaderManager = getLoaderManager();

        loaderManager.initLoader(BOOK_LOADER_ID, null, this);
    }

    private boolean isNetworkConnected(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isConnected = false;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        Toast.makeText(this, String.valueOf(isConnected), Toast.LENGTH_SHORT).show();
        return isConnected;
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int id, Bundle args) {
        return new BookLoader(this, intentParamSearchTerm);
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> data) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        mAdapter.clear();
        if (data != null && !data.isEmpty()) {
            mEmptyStateTextView.setVisibility(View.GONE);
            mAdapter.addAll(data);
        }else{
            mEmptyStateTextView.setVisibility(View.VISIBLE);
            mEmptyStateTextView.setText(getString(R.string.noBooksFound));

            if(!isNetworkConnected()){
                mEmptyStateTextView.setText(getString(R.string.noNetworkConnectivity));
            }
        }
        layoutResults = (RelativeLayout) findViewById(R.id.layoutResults);
        layoutResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {
        // Loader reset, so we can clear out our existing data.
        mEmptyStateTextView.setText("");
        mEmptyStateTextView.setVisibility(View.GONE);
        mAdapter.clear();
    }
}
