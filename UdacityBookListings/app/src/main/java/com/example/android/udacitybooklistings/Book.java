package com.example.android.udacitybooklistings;

/**
 * Created by Dan on 3/31/2017.
 */

public class Book {

    private String title;
    private String[] author;
    private String publisher;
    private int numPages;
    private String language;
    private String infoUrl;
    private String webReader;
    private int urlIconId;

    public void setTitle(String t){
        this.title = t;
    }
    public void setAuthor(String a[]){
        this.author = a;
    }
    public void setPublisher(String p){
        this.publisher = p;
    }
    public void setNumPages(int n){
        this.numPages = n;
    }
    public void setLanguage(String l){
        this.language = l;
    }
    public void setInfoUrl(String i){
        this.infoUrl = i;
    }
    public void setWebReader(String w){
        if (w.length() > 0){
            this.urlIconId = R.drawable.ic_http_black_24dp;
        }else{
            this.urlIconId = R.drawable.ic_no_http_black_24dp;
        }
        this.webReader = w;
    }
    public String getTitle(){
        return this.title;
    }
    public String[] getAuthor(){
        return this.author;
    }
    public String getPublisher(){
        return this.publisher;
    }
    public int getNumPages(){
        return this.numPages;
    }
    public String getLanguage(){
        return this.language;
    }
    public String getInfoUrl(){
        return this.infoUrl;
    }
    public String getWebReader(){
        return this.webReader;
    }
    public int getUrlIconId(){return this.urlIconId;}
}
