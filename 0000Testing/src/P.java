// Copyright (c) <<current year>> Art & Logic, Inc. All Rights Reserved.
public class P {
	
	/**
	 * 
	 * @author Dan
	 * Class usage - Shortcut for printing any message
	 * Function usage - Object is usually not instantiated,
	 * 	but statically called
	 * Function parameters - Statically-accessed method receives and prints
	 * 	a string
	 * Return values - returns nothing, prints a string to the console
	 * Special constraints - none
	 */
	
	public static void p(String s){
		System.out.println(s);
	}
}
