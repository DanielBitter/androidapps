package com.example.android.alpcpart2;

/**
 * Created by Dan on 9/30/2016.
 */
public class Constants {

//    private static final int kUpperLimitX = 8191;
//    private static final int kLowerLimitX = -8192;
//    private static final int kUpperLimitY = 8191;
//    private static final int kLowerLimitY = -8192;


    private static boolean gPenDrawing = false;
    private static boolean gPenDrawingPrevVal = false;
    private static boolean gPenWithinBounds = true;
    private static ACoordinateDO gPenLocation = new ACoordinateDO(0, 0);
    private static ACoordinateDO gMovePenToThisCoord = new ACoordinateDO(0, 0);
    private static AColorDO gLineColor = new AColorDO(0, 0, 0, 255);

//    public static int getUpperLimitX()
//    {
//        return kUpperLimitX;
//    }
//
//    public static int getLowerLimitX()
//    {
//        return kLowerLimitX;
//    }
//
//    public static int getUpperLimitY()
//    {
//        return kUpperLimitY;
//    }
//
//    public static int getLowerLimitY()
//    {
//        return kLowerLimitY;
//    }

    public static void setPenDrawing(boolean isDrawing)
    {
        gPenDrawing = isDrawing;
    }

    public static void setPenDrawingPrevVal(boolean prevVal)
    {
        gPenDrawingPrevVal = prevVal;
    }

    public static void setPenWithinBounds(boolean inBounds)
    {
        gPenWithinBounds = inBounds;
    }

    public static void setPenLocation(ACoordinateDO penLocation)
    {
        gPenLocation = penLocation;
    }

    public static void movePenToNewCoord(ACoordinateDO newCoord)
    {
        gMovePenToThisCoord = newCoord;
    }

    public static void setLineColor(AColorDO newColor)
    {
        gLineColor = newColor;
    }

    public static boolean getPenDrawing()
    {
        return gPenDrawing;
    }

    public static boolean getPenDrawingPrevVal()
    {
        return gPenDrawingPrevVal;
    }

    public static boolean getPenWithinBounds()
    {
        return gPenWithinBounds;
    }

    public static ACoordinateDO getPenLocation()
    {
        return gPenLocation;
    }

    public static ACoordinateDO getMovePenToThisCoord()
    {
        return gMovePenToThisCoord;
    }

    public static AColorDO getgLineColor()
    {
        return gLineColor;
    }

}
