// Copyright (c) <<current year>> Art & Logic, Inc. All Rights Reserved.
package com.example.android.alpcpart2;

import android.content.Context;

public class Decoder
{
	/**
	 * @author Dan
	 * Class usage - Class is instantiated to decode an encoded hex number
	 * Function usage - Object must be initialized before usage, 
	 * 	public method is the only method to be called externally
	 * Function parameters - Object receives no input, 
	 * 	public method receives an encoded hex string
	 * Return values - Public method returns the decoded signed integer
	 * 	Contructor returns nothing
	 * Special constraints - none
	 */

	int byteLength;
	int radix2;
	int radix4;
	int radix8;
	int radix16;
	int offset;
	String zero;
	String err_invalidInput;

	public Decoder(){

	}

	public void initialize(Context c)
	{
		byteLength = c.getResources().getInteger(R.integer.byteLength);
		radix2 = c.getResources().getInteger(R.integer.radix2);
		radix4 = c.getResources().getInteger(R.integer.radix4);
		radix8 = c.getResources().getInteger(R.integer.radix8);
		radix16 = c.getResources().getInteger(R.integer.radix16);
		offset = c.getResources().getInteger(R.integer.offset);
		zero = c.getString(R.string.zeroString);
		err_invalidInput = c.getString(R.string.err_invalidInput);
	}

	private String decodeHex(String hexString)
	{
		String result = "";
		String byteOneHex = decode(hexString.substring(0, byteLength));
		String byteTwoHex = decode(hexString.substring(byteLength));

		String intermediate = getIntermedFromEncoded(byteOneHex, byteTwoHex);
		int signedInt = getSignedInt(intermediate);
		result = result.concat("" + signedInt);
		return result;
	}

	private int getSignedInt(String intermediate)
	{
		int unsignedInt = Integer.parseInt(intermediate, radix2);
		int signedInt = unsignedInt - offset;
		return signedInt;
	}

	private static String getIntermedFromEncoded(String byteOne, String byteTwo)
	{
		// substring(1) to remove leading 0
		String intermediateNum = byteOne.substring(1)
				.concat(byteTwo.substring(1));
		return intermediateNum;
	}

	private String decode(String hexStr)
	{
		int hexToInt = Integer.parseInt(hexStr, radix16);
		String binaryNum = Integer.toBinaryString(hexToInt);
		binaryNum = insertPadding(binaryNum, byteLength * 4);
		return binaryNum;
	}

	private String insertPadding(String unpadded, int targetLength)
	{
		int numOfNeededZeroes = targetLength - unpadded.length();
		String padded = unpadded;
		for (int idx = 0; idx < numOfNeededZeroes; ++idx)
		{
			padded = zero.concat(padded);
		}
		return padded;
	}
	
	public String validateAndDecode(String encodedHex)
	{
		String hexString = encodedHex;
		String testInput = "";
		try
		{
			testInput = String.valueOf(Integer.parseInt(hexString, radix16));
		} 
		catch (Exception e) 
		{
			hexString = err_invalidInput;
		}
		String decodedSignedInt = "";
		if (hexString.length() > byteLength * 2)
		{
//			TextView displayResult = (TextView) findViewById(R.id.displayEncodeResults);
//			displayResult.setText(displayResult.getText().toString().concat("[validateAndDecode]" + "Error: invalid input"));
//			P.p(");
		} 
		else 
		{
			hexString = insertPadding(hexString, byteLength * 2);
			decodedSignedInt = decodeHex(hexString);
		}
		return decodedSignedInt;
	}
}
