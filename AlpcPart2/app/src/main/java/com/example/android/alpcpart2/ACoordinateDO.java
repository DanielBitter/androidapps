// Copyright (c) <<current year>> Art & Logic, Inc. All Rights Reserved.
package com.example.android.alpcpart2;

/**
 * @author Dan
 * Class usage - Generic Data Object for  coordinates
 * Function usage - Object can be initialized when created,
 * 	attributes are adjusted either immediately or in due time
 * Function parameters - Object either receives no input or
 * 	receives initial coordinates,
 * 	attributes can be adjusted with getter/setter methods
 * 	isCrossedInBounds is initialized to -1, and explicity set to 0 or 1 when needed
 * Return values - Getter methods return the corresponding attribute.
 * 	Setter methods return nothing.
 * 	Contructor returns nothing
 * Special constraints - none
 */

public class ACoordinateDO
{
	private int x = -2;
	private int y = -2;
	private int isCrossedInBounds = -2;


	public ACoordinateDO()
	{
		this.setIsCrossedInBounds(-1);
	}

	public ACoordinateDO(int x, int y)
	{
		this.setX(x);
		this.setY(y);
		this.setIsCrossedInBounds(-1);
	}

	public void setX(int incomingX)
	{
		this.x = incomingX;
	}

	public void setY(int incomingY)
	{
		this.y = incomingY;
	}

	public void setIsCrossedInBounds(int crossed)
	{
		this.isCrossedInBounds = crossed;
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}

	public int getIsCrossedInBounds()
	{
		return this.isCrossedInBounds;
	}

	public String getFormattedCoord()
	{
		return "(" + this.x + ", " + this.y + ")";
	}
}
