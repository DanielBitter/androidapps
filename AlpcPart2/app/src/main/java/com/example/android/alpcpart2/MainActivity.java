package com.example.android.alpcpart2;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.TreeMap;

import com.example.android.alpcpart2.Constants;

public class MainActivity extends AppCompatActivity {

    int limit_upperX;
    int limit_lowerX;
    int limit_upperY;
    int limit_lowerY;
    String[] commands;
    String[] commandIds;
    Decoder decoder;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        limit_upperX = getResources().getInteger(R.integer.kUpperLimitX);
        limit_lowerX = getResources().getInteger(R.integer.kLowerLimitX);
        limit_upperY = getResources().getInteger(R.integer.kUpperLimitY);
        limit_lowerY = getResources().getInteger(R.integer.kLowerLimitY);

        commands = new String[]{
                getString(R.string.cmd_clr),
                getString(R.string.cmd_pen),
                getString(R.string.cmd_colr),
                getString(R.string.cmd_move)
        };

        commandIds = new String[]{
                getString(R.string.id_clr),
                getString(R.string.id_pen),
                getString(R.string.id_colr),
                getString(R.string.id_move)
        };
        
        decoder = new Decoder();
        decoder.initialize(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void clearUserInput(View view)
    {
        EditText userInput = (EditText)
                findViewById(R.id.encodeUserInput);
        userInput.setText("");
        TextView displayResult = (TextView)
                findViewById(R.id.displayEncodeResults);
        displayResult.setText(" ");
    }

    public void execute(View view)
    {
        EditText userInput = (EditText)
                findViewById(R.id.encodeUserInput);
        TextView displayResult = (TextView)
                findViewById(R.id.displayEncodeResults);
        displayResult.setText(" ");

        String userDefinedDataStream = userInput.getText().toString();
        executeCmds(userDefinedDataStream);
    }

    public void executeCmds(String cmds)
    {
        ArrayList<ACommandDO> listOfCmds = createCmdDO(cmds);

        for (ACommandDO cmd : listOfCmds)
        {
            String opcode = cmd.getCommandOpcode();
            if(commands[0].equalsIgnoreCase(opcode)){
                reset();
            }else if(commands[1].equalsIgnoreCase(opcode)){
                if (-1 < cmd.getConsoleOutput().indexOf(getString(R.string.penDir_up)))
                {
                    togglePenDrawing(false);
                }
                else
                {
                    togglePenDrawing(true);
                }
            }else if(commands[2].equalsIgnoreCase(opcode)){
                if (cmd.getValidStatus())
                {
                    String parmList[] = cmd.getParameters();
                    String colorCodeList = "";
                    String separator = " ";
                    for (String parm : parmList)
                    {
                        colorCodeList = colorCodeList.
                                concat(parm + separator);
                    }

                    cmd.setConsoleOutput(colorCodeList.trim());
                }
            }else if(commands[3].equalsIgnoreCase(opcode)){
                if (cmd.getValidStatus())
                {
                    String consoleOutputCoords =
                            buildDrawingCoordinates(cmd.getCoordArray());
                    cmd.setConsoleOutput(consoleOutputCoords);
                }
            }


            TextView displayResult = (TextView)
                    findViewById(R.id.displayEncodeResults);
            displayResult.setText(
                    displayResult.getText().toString().
                            concat(cmd.getConsoleOutput()));
        }
    }

    public void reset()
    {
        togglePenDrawing(false);
        setPenCoords(new ACoordinateDO(0, 0));
        setColor(0, 0, 0, 255);
    }

    public void togglePenDrawing(boolean shouldDraw)
    {
        if (shouldDraw)
        {
            Constants.setPenDrawing(true);//gPenDrawing = true;
        }
        else
        {
            Constants.setPenDrawing(false);//gPenDrawing = false;
        }
    }

    public String decodeAndSetColor(String[] parameters)
    {
        if(decoder == null){
            decoder = new Decoder();
            decoder.initialize(this);
        }
        String result = "";
        String separator = " ";

        for (String colorParam : parameters)
        {
            result = result.concat(decoder.validateAndDecode(colorParam) +
                    separator);
        }
        result = result.trim();
        return result;
    }

    public void setColor(int r, int g, int b, int a)
    {
        Constants.setLineColor(new AColorDO(r, g, b, a));//gLineColor.setColor(r, g, b, a);
    }

    public String buildDrawingCoordinates(ArrayList<ACoordinateDO> coordArray)
    {
        String consoleOutput = "";
        ACoordinateDO lastCoordEvald = new ACoordinateDO();
        for (ACoordinateDO coord : coordArray)
        {
            coord = updatePenCoords(coord);
            if (Constants.getPenDrawing())//(gPenDrawing)
            {
                if(coord != null)
                {   //coord is null if coordArray element is within bounds
                    //otherwise, coord is the calculated enter/exit point
                    // along the boundary
                    if(coord.getIsCrossedInBounds() == 0)
                    {
                        //line going out = pen up
                        consoleOutput = consoleOutput.concat(coord.getFormattedCoord() + ";");
                        consoleOutput = consoleOutput
                                .concat("\n")
                                .concat(getString(R.string.penDir_upDesc))
                                .concat(";");
                    }
                    else
                    {
                        //line coming in = pen down
                        consoleOutput = consoleOutput.concat("\n")
                            .concat(getString(R.string.id_move))
                            .concat(coord.getFormattedCoord().trim())
                            .concat(";");
                        consoleOutput = consoleOutput
                                .concat("\n")
                                .concat(getString(R.string.penDir_downDesc))
                                .concat(";");
                    }
                }
                else if(penWithinBounds())
                {
                    consoleOutput = consoleOutput.concat(getFormattedPenCoords());
                }

            }
            lastCoordEvald = coord;
        }
        if (!Constants.getPenDrawing()) //gPenDrawing)
        {
            consoleOutput = getFormattedPenCoords();
        }
        if (lastCoordEvald != null)
        {
            ACoordinateDO lastCoordInArray = coordArray.get(coordArray.size() - 1);
            if(!lastCoordEvald.getFormattedCoord()
                    .equalsIgnoreCase(
                            lastCoordInArray
                                    .getFormattedCoord()))
            {
                consoleOutput = consoleOutput.concat("\n")
                        .concat(getString(R.string.id_move))
                        .concat(getFormattedPenCoords().trim());
            }
        }
        return consoleOutput.trim();
    }

    public String getFormattedPenCoords()
    {
//        return "(" + gPenLocation.getX() + ", " +
//                gPenLocation.getY() + ")";
        return "(" + Constants.getPenLocation().getX() + ", " +
                Constants.getPenLocation().getY() + ")";
    }

    public void setPenCoords(ACoordinateDO coord)
    {
        Constants.getPenLocation().setX(coord.getX());
        Constants.getPenLocation().setY(coord.getY());
    }

    public ACoordinateDO updatePenCoords(ACoordinateDO coord)
    {
        int dx = coord.getX();
        int dy = coord.getY();

        Constants.getMovePenToThisCoord().setX(Constants.getPenLocation().getX() + dx);
        Constants.getMovePenToThisCoord().setY(Constants.getPenLocation().getY() + dy);

        boolean penIsOutOfBounds =
                (limit_upperX <= Constants.getMovePenToThisCoord().getX()) ||
                (limit_lowerX >= Constants.getMovePenToThisCoord().getX()) ||
                (limit_upperY <= Constants.getMovePenToThisCoord().getY()) ||
                (limit_lowerY >= Constants.getMovePenToThisCoord().getY());
        boolean penCrossingIntoBounds =
                (0 > dx && limit_upperX <= Constants.getPenLocation().getX()
                        && limit_upperX > Constants.getMovePenToThisCoord().getX()) ||
                (0 < dx && limit_lowerX >= Constants.getPenLocation().getX()
                        && limit_lowerX < Constants.getMovePenToThisCoord().getX()) ||
                (0 > dy && limit_upperY <= Constants.getPenLocation().getY()
                        && limit_upperY > Constants.getMovePenToThisCoord().getY()) ||
                (0 < dy && limit_lowerY >= Constants.getPenLocation().getY()
                        && limit_lowerY < Constants.getMovePenToThisCoord().getY());
        boolean penIsWithinBounds =
                ((0 < dx && limit_upperX > Constants.getMovePenToThisCoord().getX()) ||
                 (0 > dx && limit_lowerX < Constants.getMovePenToThisCoord().getX())) &&
                ((0 < dy && limit_upperY > Constants.getMovePenToThisCoord().getY()) ||
                 (0 > dy && limit_lowerY < Constants.getMovePenToThisCoord().getY()));

        ACoordinateDO borderPoint = null;
        TextView displayResult = (TextView)
                findViewById(R.id.displayEncodeResults);
        if (penIsOutOfBounds && penWithinBounds())
        {
            updatePenWithinBounds(false);
            borderPoint = calculateEntranceExitPoint(Constants.getPenLocation(),
                    Constants.getMovePenToThisCoord(), getString(R.string.penDir_out));
            borderPoint.setIsCrossedInBounds(0);
        }
        if(penCrossingIntoBounds && !penWithinBounds())
        {
            updatePenWithinBounds(true);
            borderPoint = calculateEntranceExitPoint(Constants.getPenLocation(),
                    Constants.getMovePenToThisCoord(), getString(R.string.penDir_in));
            borderPoint.setIsCrossedInBounds(1);
        }
        if(penIsWithinBounds && !penWithinBounds())
        {
            updatePenWithinBounds(true);
            return null;
        }

        Constants.getPenLocation().setX(Constants.getMovePenToThisCoord().getX());
        Constants.getPenLocation().setY(Constants.getMovePenToThisCoord().getY());

        return borderPoint;
    }

    public ACoordinateDO calculateEntranceExitPoint(ACoordinateDO location,
                  ACoordinateDO updatedLocation, String direction)
    {
        int dx = 1;
        int dy = 0;
        String penDirOut = getString(R.string.penDir_out);
        String penDirIn = getString(R.string.penDir_in);
        ACoordinateDO enterExitPoint = new ACoordinateDO();
        if(direction.equals(penDirOut))
        {
            dx = updatedLocation.getX() - location.getX();
            dy = updatedLocation.getY() - location.getY();
        }
        else if(direction.equals(penDirIn))
        {
            dx = location.getX() - updatedLocation.getX();
            dy = location.getY() - updatedLocation.getY();
        }
        double slope = 0.0;
        try{
            slope = (double) dy / dx;
        }
        catch(Exception e)
        {
            //vertical line
        }

        if(direction.equals(penDirIn))
        {
            ACoordinateDO switchCoords = location;
            location = updatedLocation;
            updatedLocation = switchCoords;
        }

        if( limit_upperX < updatedLocation.getX())
        {
            enterExitPoint.setX(limit_upperX);
            if(0 < slope)
            {
                int y = (int) (location.getY() +
                        ((limit_upperX - location.getX()) * slope));
                enterExitPoint.setY(y);
            }
            else
            {
                int y = (int) (location.getY() -
                        ((limit_lowerX + location.getX()) * slope));
                enterExitPoint.setY(y);
            }
        }
        else if(limit_lowerX > updatedLocation.getX())
        {
            enterExitPoint.setX(limit_lowerX);
            if(0 < slope)
            {
                enterExitPoint.setY((int) (slope * limit_upperY));
            }
            else
            {
                enterExitPoint.setY((int) (slope * limit_lowerY));
            }
        }
        else if(limit_upperY < updatedLocation.getY())
        {
            enterExitPoint.setY(limit_upperY);
            if(0 < slope)
            {
                enterExitPoint.setX((int) (slope * limit_upperX));
            }
            else
            {
                enterExitPoint.setX((int) (slope * limit_lowerX));
            }
        }
        else if(limit_lowerY > updatedLocation.getY())
        {
            enterExitPoint.setY(limit_lowerY);
            if(0 < slope)
            {
                enterExitPoint.setX((int) (slope * limit_lowerX));
            }
            else
            {
                enterExitPoint.setX((int) (slope * limit_upperX));
            }
        }

        return enterExitPoint;

    }


    public boolean penWithinBounds()
    {
        if (Constants.getPenWithinBounds())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void updatePenWithinBounds(boolean isInBounds)
    {
        Constants.setPenWithinBounds(isInBounds);
    }

    public ArrayList<ACommandDO> createCmdDO(String commands)
    {
        ArrayList<ACommandDO> listOfCmds = new ArrayList<ACommandDO>();
        ArrayList<String> listOfCmdStrings = new ArrayList<String>();
        listOfCmdStrings = getArrayListOfCmds(commands);
        int byteLen = getResources().getInteger(R.integer.byteLength);
        for (String cmd : listOfCmdStrings)
        {
            String opcode = cmd.substring(0, byteLen);
            String cmdString = cmd.substring(byteLen);
            ACommandDO cDO = new ACommandDO(this);
            cDO.setCmdOpcode(opcode);
            cDO.setCmdString(cmdString);
            cDO.populateFields();

            listOfCmds.add(cDO);
        }

        return listOfCmds;
    }

    public ArrayList<String> getArrayListOfCmds(String cmd)
    {
        ArrayList<String> cmdArrayList = new ArrayList<String>();
        String newCmd = cmd;
        String futureCmd = "";
        int cmdOffset = 0;
        String next_error = getString(R.string.err_error);
        String next_none = getString(R.string.err_none);

        while (0 < newCmd.length())
        {
            String nextCmd = getNextCmd(newCmd);
            String nextCmdValue = nextCmd.split(":")[0];
            String nextCmdKey = nextCmd.split(":")[1];
            cmdOffset = nextCmdValue.length();
            int nextCmdIdx = -2;
            if (!next_error.equals(nextCmdKey)  && !next_none.equals(nextCmdKey))
            {
                nextCmdIdx = Integer.parseInt(nextCmdKey);
                futureCmd = newCmd.substring(nextCmdIdx + cmdOffset);
            }

            int futureCmdIdx = -2;
            futureCmd = getNextCmd(futureCmd);
            String futureCmdValue = futureCmd.split(":")[0];
            String futureCmdKey = futureCmd.split(":")[1];
            if (!futureCmdKey.equals(next_error) &&
                    !futureCmdKey.equals(next_none))
            {
                futureCmdIdx = Integer.parseInt(futureCmdKey);
            }

            String command = newCmd.substring(nextCmdIdx);
            if (0 <= futureCmdIdx)
            {
                command = newCmd.substring(nextCmdIdx,
                        futureCmdIdx + cmdOffset);
            }
            cmdArrayList.add(command);
            newCmd = newCmd.substring(command.length());
        }
        return cmdArrayList;
    }

    public String getNextCmd(String cmd)
    {
        String next_error = getString(R.string.err_error);
        String next_none = getString(R.string.err_none);
        String nextCmd = next_none.concat(":").concat(next_none);
        int cmdIdx = -2;
        TreeMap sortedCmdMap = new TreeMap();

        int idxClear = cmd.indexOf(commands[0]);
        int idxPenDown = cmd.indexOf(commands[1]);
        int idxColor = cmd.indexOf(commands[2]);
        int idxMovePen = cmd.indexOf(commands[3]);

        sortedCmdMap.put(idxClear, commands[0]);
        sortedCmdMap.put(idxPenDown, commands[1]);
        sortedCmdMap.put(idxColor, commands[2]);
        sortedCmdMap.put(idxMovePen, commands[3]);

        sortedCmdMap.remove(-1);
        if (0 < sortedCmdMap.size())
        {
            try
            {
                cmdIdx = (Integer) sortedCmdMap.firstKey();
                nextCmd = sortedCmdMap.get(cmdIdx).toString();
                nextCmd = nextCmd.concat(":" + cmdIdx);
            }
            catch (Exception e)
            {
                nextCmd = next_error.concat(":").concat(next_error);
                String errorString = "\n"
                        .concat(getString(R.string.err_processErr))
                        .concat(String.valueOf(cmdIdx));
                TextView displayResult = (TextView)
                        findViewById(R.id.displayEncodeResults);
                displayResult.setText(displayResult.getText().toString()
                        .concat(errorString));
            }
        }

        return nextCmd;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.android.alpcpart2/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.android.alpcpart2/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
