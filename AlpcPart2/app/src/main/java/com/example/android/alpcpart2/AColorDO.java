// Copyright (c) <<current year>> Art & Logic, Inc. All Rights Reserved.
package com.example.android.alpcpart2;

/**
 * @author Dan
 * Class usage - Data Object for line color information
 * Function usage - Object must be initialized when created, 
 * 	and attributes can then be adjusted
 * Function parameters - Object receives ints r, g, b, a 
 * 	for red, blue, green, alpha attributes
 * Return values - Getter methods return the corresponding attribute. 
 * 	Setter methods return nothing. 
 * 	Contructor returns nothing
 * Special constraints - none
 * RGBA values of -2 are for debugging purposes, these values should
 * always immediately be overwritten
 */

public class AColorDO
{
	private int r = -2;
	private int g = -2;
	private int b = -2;
	private int a = -2;

	public AColorDO(int red, int green, int blue, int alpha)
	{
		setColor(red, green, blue, alpha);
	}

	public void setColor(int red, int green, int blue, int alpha)
	{
		this.r = red;
		this.g = green;
		this.b = blue;
		this.a = alpha;
	}

	public void setRed(int red)
	{
		this.r = red;
	}

	public void setGreen(int green)
	{
		this.g = green;
	}

	public void setBlue(int blue)
	{
		this.b = blue;
	}

	public void setAlpha(int alpha)
	{
		this.a = alpha;
	}

	public int getRed()
	{
		return this.r;
	}

	public int getGreen()
	{
		return this.g;
	}

	public int getBlue()
	{
		return this.b;
	}

	public int getAlpha()
	{
		return this.a;
	}
}
