// Copyright (c) <<current year>> Art & Logic, Inc. All Rights Reserved.
package com.example.android.alpcpart2;
import android.content.Context;

import java.util.ArrayList;

/**
 * @author Dan
 * Class usage - Generic Data Object for  input commands
 * Function usage - Object can be initialized when created, 
 * 	attributes are adjusted either immediately or in due time
 * Function parameters - Object receives no input, rather,
 * 	attributes are adjusted with getter/setter methods 
 * Return values - Getter methods return the corresponding attribute. 
 * 	Setter methods return nothing. 
 * 	Contructor returns nothing
 * Special constraints - none
 */

public class ACommandDO
{
	private String commandOpcode = null;
	private String commandString = null;
	private String commandId = null;
	private String[] parameters = null;
	private String consoleOutput = null;
	private boolean valid = false;
	private ArrayList<ACoordinateDO> coordArray = null;

	String[] commands;
	String[] commandIds;
	String parms_none;
	String[] penDir;
	String colorInfo;
	int cmdStrLen;
	String separater;

	public ACommandDO(Context c)
	{
		commands = new String[]{
				c.getString(R.string.cmd_clr),
				c.getString(R.string.cmd_pen),
				c.getString(R.string.cmd_colr),
				c.getString(R.string.cmd_move)
		};
		
		commandIds = new String[]{
				c.getString(R.string.id_clr),
				c.getString(R.string.id_pen),
				c.getString(R.string.id_colr),
				c.getString(R.string.id_move)
		};
		parms_none = c.getString(R.string.parms_none);
		colorInfo = c.getString(R.string.colorInfo);
		cmdStrLen = c.getResources().getInteger(R.integer.cmdStringLengths);
		separater = c.getString(R.string.stringSeparator);

		penDir = new String[]{
				c.getString(R.string.pen_up),
				c.getString(R.string.pen_down)
		};
	}

	public void setCmdOpcode(String op)
	{
		this.commandOpcode = op;
	}

	public void setCmdString(String cmdStr)
	{
		this.commandString = cmdStr;
	}

	public void setConsoleOutput(String output)
	{
		this.consoleOutput = "\n" + this.commandId + " " + output + ";";
	}

	public String getCommandOpcode()
	{
		return this.commandOpcode;
	}

	public String getCommandString()
	{
		return this.commandString;
	}

	public String getCommandId()
	{
		return this.commandId;
	}

	public String[] getParameters()
	{
		return this.parameters;
	}

	public String getConsoleOutput()
	{
		return this.consoleOutput;
	}

	public boolean getValidStatus()
	{
		return this.valid;
	}

	public ArrayList<ACoordinateDO> getCoordArray()
	{
		return this.coordArray;
	}

	public void populateFields()
	{
		String opcode = this.commandOpcode;
		if (opcode.equals(commands[0])) 
		{
			this.commandId = commandIds[0];
			this.parameters = new String[] {parms_none};
			this.consoleOutput = "\n" + this.commandId + ";";
			this.valid = true;

		} 
		else if (opcode.equals(commands[1])) 
		{
			this.commandId = commandIds[1];
			this.parameters = new String[] {this.commandString};

			int penDirCode = -1;
			try
			{
				penDirCode = Integer.valueOf(this.commandString.substring(1));
			} 
			catch (Exception e) 
			{
//				TextView displayResult = (TextView) findViewById(R.id.displayEncodeResults);
//				displayResult.setText(displayResult.getText().toString().concat("[populateFields]" + "Error - param not a number"));
//				P.p();
			}
			String direction = penDir[0];
			if (penDirCode != 0)
			{
				direction = penDir[penDirCode];
			}

			this.consoleOutput = "\n" + this.commandId + direction + ";";
			this.valid = true;

		} 
		else if (opcode.equals(commands[2])) 
		{
			this.commandId = commandIds[2];
			this.setParameters();
			if (this.parameters.length == 4) 
			{
				this.valid = true;
			}
			else
			{
				this.valid = false;
			}
			this.consoleOutput = "\n" + this.commandId + colorInfo + ";";

		} 
		else if (opcode.equals(commands[3])) 
		{
			this.commandId = commandIds[3];
			this.setParameters();
			if (this.parameters.length % 2 == 0) 
			{
				this.valid = true;
			} 
			else 
			{
				this.valid = false;
			}
			String formattedOutputString = "";
			if (this.valid) 
			{
				this.coordArray = new ArrayList<ACoordinateDO>();
				int tempXvalue = 0;
				for (int i = 0; i < this.parameters.length; ++i) 
				{
					if ((i + 2) % 2 == 0) 
					{
						tempXvalue = Integer.parseInt(this.parameters[i]);
					} 
					else 
					{
						ACoordinateDO coordDO = new ACoordinateDO();
						coordDO.setX(tempXvalue);
						coordDO.setY(Integer.parseInt(this.parameters[i]));
						this.coordArray.add(coordDO);
						formattedOutputString = formattedOutputString
								.concat(coordDO.getFormattedCoord());
					}

				}
				formattedOutputString = formattedOutputString.trim();
			}
			this.consoleOutput = "\n " + this.commandId + " " + formattedOutputString + ";";

		} 
		else 
		{
			assert(false);
		}
	}

	public String getDecodedNumber(String encodedHex)
	{
		Decoder decoder = new Decoder();
		String result = decoder.validateAndDecode(encodedHex);
		return result;
	}

	private void setParameters()
	{
		String newCommandString = "";
		while (this.commandString.length() >= cmdStrLen)
		{
			String coordHex = this.commandString.substring(0, cmdStrLen);
			String coordStr = getDecodedNumber(coordHex);
			newCommandString = newCommandString.concat(coordStr + separater);
			this.commandString = this.commandString.substring(cmdStrLen);
		}
		if (this.commandString.length() > 0) 
		{
			newCommandString = newCommandString.concat(this.commandString);
		}
		String lastCharacer = newCommandString.
				substring(newCommandString.length() - 1);
		if (lastCharacer.equals(separater)) 
		{
			newCommandString = newCommandString.
					substring(0, newCommandString.length() - 1);
		}

		this.parameters = newCommandString.split(separater);
	}

}
