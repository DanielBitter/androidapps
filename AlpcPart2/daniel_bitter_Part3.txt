1. Tell us about something that you recently learned (or are currently learning). How do you approach
learning a new skill?
Currently learning Android app development - I started with the Udacity "Android Development for Beginners" course, which I have a few video lessons left. When I've mastered the basics, I'll enroll in the "Android Basics: Multi-screen Apps" course, and finally the "Android Basics Nanodegree by Google" program. I'd like to have a solid foundation before starting a paid course.

2. Describe the differences between projects you've been involved with (or seen) that have succeeded,
and those that have not been successful.
It's all about communication and managing expectations. I generally find that results don't matter nearly as much as what those results are expected to be. Issues and shortcomings can be mitigated with a team, but only if the issues are known and clearly explained. 

3. The languages, tools, and technologies that we use as developers are always changing. What
specific languages/framworks/platforms should developers be learning now to be ready for the next
few years?
I think the languages won't change much, but rather be adapted if anything. Android and iOS are the newest "programming languages," but even they rely on Java and C. Platforms and frameworks to keep an eye on definitely include autonomous vehicles, especially the sensors they use to keep humans safe. Cloud computing is an easy topic to pick, but I think the bigger boom to come relates to data management - adding user after user to a cloud-based system requires lots of server time and storage costs. Advancements in compression, encyptions, and computer hardware will be the driving forces behind this topic.

4. Discuss the most effective team you've been a part of. What made it so? What was your role on the
team?
The first example that jumps to mind is my 3-person team in Intro to Programming in college. Our final project was to recreate the class game “Pong” with Java. One team member was significantly more experienced with programming, so he was our team lead. We all programmed the different logic and classes according to difficulty, and he explained the more advanced code to ensure we had a deep understand of the event flow. A couple years later as an upperclassman, I was able to do the same for the current freshman - acting as a mentor for their video game final project.

5. What specific approaches or techniques do you use in your code to make sure that it's bug-free and
performant?
I’ve found that nearly all issues arise when starting or ending an operation - usually, the “meat” of an operation is easy to write. Things like validating user input and other variables, catching & recovering from exceptions, and finalizing data have had the most potential for causing bugs. I try to think about the scenarios that both malicious and naive users could create, and build my code to handle them as well as possible.