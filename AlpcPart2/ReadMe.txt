Project Name: AlpcPart2

Author: Dan Bitter

Description: Job application challenge #2 from Art & Logic programming. Application
 receives user input in the text field as a single line of characters and evaluates
to find any relevant commands. These commands simulate a pen drawing on a canvas,
and can move the pen, lift or lower to start or stop drawing, and clear the canvas.
Commands are listed in a TextView in order.

Sample commands:
F0!80@A0####@@@@!!!!$$$$C0$$$$%%%%
F0A04000417F4000417FC040004000804001C05F205F20804000
F090400047684F5057384000A040004000417F417FC040004000804001C05F204000400001400140400040007E405B2C4000804000
F0A0417F40004000417FC067086708804001C0670840004000187818784000804000
F0A0417F41004000417FC067086708804001C067082C3C18782C3C804000

Deployment: The application can be loaded to Android Studio and built for an 
emulator or attached device. 

Requirements: Currently targeted for min sdk version 15

Contact/bug reports/feature requests:
bitter.daniel@gmail.com
linkedin.com/in/danielbitter

File list:
/app/manifests/AndroidManifest.xml
/app/java/com.example.android.alpcpart2/AColorDO.java
/app/java/com.example.android.alpcpart2/ACommandDO.java
/app/java/com.example.android.alpcpart2/ACoordinateDO.java
/app/java/com.example.android.alpcpart2/Constants.java
/app/java/com.example.android.alpcpart2/Decoder.java
/app/java/com.example.android.alpcpart2/MainActivity.java
/app/res/layout/activity_main.xml
/app/res/mipmap/ic_launcher.png
/app/res/values/colors.xml
/app/res/values/dimens.xml
/app/res/values/integers.xml
/app/res/values/strings.xml
/app/res/values/styles.xml