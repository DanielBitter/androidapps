package com.example.android.quakereport;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.List;

/**
 * Created by Dan on 3/21/2017.
 */

public class EarthquakeLoader
    extends AsyncTaskLoader<List<Earthquake>> {
    private static final String LOG_TAG = EarthquakeLoader.class.getName();
    private String url;

    public EarthquakeLoader(Context context, String urlArg) {
        super(context);
        this.url = urlArg;
        Log.v(LOG_TAG,"EarthquakeLoader constructor()");
    }

    @Override
    public List<Earthquake> loadInBackground() {
        Log.v(LOG_TAG,"loadInBackground()");
        if(url == null || url.length() < 1){
            return null;
        }
        List<Earthquake> earthquakeData = QueryUtils.fetchEarthquakeData(url);
        return earthquakeData;
    }

    @Override
    protected void onStartLoading() {
        Log.v(LOG_TAG,"onStartLoading()");
        forceLoad();
    }
}
