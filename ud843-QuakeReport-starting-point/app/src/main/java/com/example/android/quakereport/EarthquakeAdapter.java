package com.example.android.quakereport;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dan on 2/25/2017.
 * https://www.sitepoint.com/custom-data-layouts-with-your-own-android-arrayadapter/
 */

public class EarthquakeAdapter extends ArrayAdapter<Earthquake> {
    private Context context;
    private List<Earthquake> earthquakeData;

    public EarthquakeAdapter(Context ct, List<Earthquake> eqs){
        super(ct, 0, eqs);
        this.context = ct;
        this.earthquakeData = eqs;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        double eqM = earthquakeData.get(position).getMagnitude();
        String eqL = earthquakeData.get(position).getLocation();
        String eqLD = earthquakeData.get(position).getLocationDetails();
        String eqD = earthquakeData.get(position).getDate();
        long eqT_long = earthquakeData.get(position).getUnixEpochTime();
        String eqT = earthquakeData.get(position).getTime(); //String.valueOf(eqT_long);

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.earthquake_item, null);
        }

        TextView magnitude = (TextView) convertView.findViewById(R.id.magnitude);
        TextView location = (TextView) convertView.findViewById(R.id.loc);
        TextView locationDetails = (TextView) convertView.findViewById(R.id.locDetails);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView time = (TextView) convertView.findViewById(R.id.time);

        GradientDrawable magnitudeCircle = (GradientDrawable) magnitude.getBackground();
        int magnitudeColor = getMagnitudeColor(eqM);
        magnitudeCircle.setColor(magnitudeColor);

        magnitude.setText(String.valueOf(eqM));
        location.setText(eqL);
        locationDetails.setText(eqLD);
        date.setText(eqD);
        time.setText(eqT);

        return convertView;
    }

    public int getMagnitudeColor(double magnitude){
        int magInt = (int) Math.floor(magnitude);
        int magColor = -1;

        switch(magInt){
            case 0:
            case 1:
                magColor = context.getColor(R.color.magnitude1);
                break;
            case 2:
                magColor = context.getColor(R.color.magnitude2);
                break;
            case 3:
                magColor = context.getColor(R.color.magnitude3);
                break;
            case 4:
                magColor = context.getColor(R.color.magnitude4);
                break;
            case 5:
                magColor = context.getColor(R.color.magnitude5);
                break;
            case 6:
                magColor = context.getColor(R.color.magnitude6);
                break;
            case 7:
                magColor = context.getColor(R.color.magnitude7);
                break;
            case 8:
                magColor = context.getColor(R.color.magnitude8);
                break;
            case 9:
                magColor = context.getColor(R.color.magnitude9);
                break;
            case 10:
                magColor = context.getColor(R.color.magnitude10plus);
                break;
            default:
                magColor = context.getColor(R.color.magnitude1);
                break;
        }

        return magColor;
    }

}
