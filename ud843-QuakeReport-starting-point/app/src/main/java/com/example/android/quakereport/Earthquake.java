package com.example.android.quakereport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Dan on 2/25/2017.
 * http://stackoverflow.com/questions/17432735/convert-unix-time-stamp-to-date-in-java
 */

public class Earthquake {
    private double magnitude;
    private String location;
    private String locationDetails;
    private long unixEpochTime;
    private String date;
    private String time;
    private String url;

    public Earthquake(){    }

    public Earthquake(double m, String l, String ld, long uet, String d, String t, String u){
        this.magnitude = m;
        this.location = l;
        this.locationDetails = ld;
        this.unixEpochTime = uet;
        this.date = d;
        this.time = t;
        this.url = u;
    }

    public void setMagnitude(double mag){this.magnitude = mag;}
    public void setLocation(String loc){this.location = loc;}
    public void setLocationDetails(String dtls){this.locationDetails = dtls;}
    public void setUnixEpochTime(long uet){this.unixEpochTime = uet;}
    public void setDate(String dt){this.date = dt;}
    public void setTime(String tm){this.time = tm;}
    public void setUrl(String u){this.url = u;}

    public double getMagnitude(){return this.magnitude;}
    public String getLocation(){return this.location;}
    public String getLocationDetails(){return this.locationDetails;}
    public long getUnixEpochTime(){return this.unixEpochTime;}
    public String getDate(){
        if(this.unixEpochTime > 0 && this.date == null){
                convertUnixTimeToDate(this.getUnixEpochTime());
        }
        return this.date;
    }
    public String getTime(){
        if(this.unixEpochTime > 0 && this.time == null){
            convertUnixTimeToDate(this.getUnixEpochTime());
        }
        return this.time;
    }
    public String getUrl(){return this.url;}

    private void convertUnixTimeToDate(long unixSeconds){
        String convertedDate = "convertedDate";

        Date date = new Date(unixSeconds);//*1000L); // *1000 is to convert seconds to milliseconds if needed
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm a"); // the format of your date yyyy-MM-dd HH:mm:ss z
//        sdf.setTimeZone(TimeZone.getTimeZone("GMT-0")); // timezone was creating an offset
        convertedDate = sdf.format(date);

        String dateSub = convertedDate.substring(0,12);
        String timeSub = convertedDate.substring(13);
        this.setDate(dateSub);
        this.setTime(timeSub);

        Date dateObject = new Date(unixSeconds);
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        String formattedDate = dateFormat.format(dateObject);

        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        String formattedTime = timeFormat.format(dateObject);

        boolean datesSame = formattedDate.equalsIgnoreCase(dateSub);
        boolean timesSame = formattedTime.equalsIgnoreCase(timeSub);

        if(datesSame == timesSame){} //for debugging
    }

}
