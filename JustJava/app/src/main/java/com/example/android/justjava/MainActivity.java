package com.example.android.justjava;
/**
 * Add your package below. Package name can be found in the project's AndroidManifest.xml file.
 * This is the package name our example uses:
 *
 * package com.example.android.justjava;
 */

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public TextView quantityTextView;
    public TextView priceTextView;
    public TextView confirmationTextView;
    public Button cancelBtn ;
    public CheckBox whippedCream;
    public String currOrder;
    public int currOrderInt;
    int whippedCreamPrice = 2;
    int coffeePrice=4;
    int unitPrice = coffeePrice;
    int total;
    String orderBeingPreparedMsg;
    String coffeeOrderBeingPreparedMsg = "Your coffee order is being prepared!";
    String coffeeWhipCreamOrderBeingPreparedMsg = "Your coffee and whipped cream order is being prepared!";


    private Handler mHandler = new Handler();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        priceTextView = (TextView) findViewById(R.id.priceTotal_text_view);
        confirmationTextView = (TextView) findViewById(R.id.confirmation_text_view);
        cancelBtn = (Button) findViewById(R.id.cancelBtnId);
        whippedCream = (CheckBox) findViewById(R.id.checkBoxToppingWhippedCream);
    }

    public void submitOrder(View view) {
        showSubmitOrderMsg();
        displayCancelBtn(true);
        //submit order to backend
    }

    public void cancelOrder(View view) {
        updateNumCoffees(0);
        whippedCream.setChecked(false);
        confirmationTextView = (TextView) findViewById(R.id.confirmation_text_view);
        confirmationTextView.setText("Order cancelled");
        confirmationTextView.setVisibility(View.VISIBLE);

        mHandler.postDelayed(new Runnable() {
            public void run() {
                displayCancelBtn(false);
            }
        }, 2000);
    }

    public void increaseOrder(View view) {
        quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        currOrder = quantityTextView.getText().toString();
        if(currOrder.indexOf(" ") > -1){currOrder = currOrder.substring(0,currOrder.indexOf(" "));}
        currOrderInt = Integer.valueOf(currOrder);
        updateNumCoffees(currOrderInt+1);
    }

    public void decreaseOrder(View view) {
        quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        currOrder = quantityTextView.getText().toString();
        if(currOrder.indexOf(" ") > -1){currOrder = currOrder.substring(0,currOrder.indexOf(" "));}
        currOrderInt = Integer.valueOf(currOrder);
        updateNumCoffees(currOrderInt-1);
    }

    private void updateNumCoffees(int number) {
        quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        String product = " coffee";
        if(number != 1){product = pluralize(product);}
        String outputString = "" + number + "" + product;
        quantityTextView.setText(outputString);
        currOrderInt = number;
        updateOrderSummary();
    }

    public String pluralize(String singular){return singular.concat("s");}

    public void showSubmitOrderMsg(){
        confirmationTextView = (TextView) findViewById(R.id.confirmation_text_view);
        if(currOrderInt > 0){  confirmationTextView.setText(orderBeingPreparedMsg);
        }else{          confirmationTextView.setText("No coffees selected");}

        confirmationTextView.setVisibility(View.VISIBLE);
    }

    public void displayCancelBtn(Boolean shouldShow){
        cancelBtn = (Button) findViewById(R.id.cancelBtnId);
        if(shouldShow){cancelBtn.setVisibility(View.VISIBLE);}
        else{cancelBtn.setVisibility(View.INVISIBLE);}
    }

    public void updatePriceDetails(){
        priceTextView = (TextView) findViewById(R.id.priceTotal_text_view);
        whippedCream = (CheckBox) findViewById(R.id.checkBoxToppingWhippedCream);

        if(currOrderInt > 0){
            total = unitPrice * currOrderInt;
            String priceString;
            if(whippedCream.isChecked()){
                priceString ="($"+coffeePrice+" + $"+whippedCreamPrice+")";
            }else{
                priceString ="$"+coffeePrice;
            }
        priceTextView.setText("$"+total+" = "+currOrderInt+" x "+priceString+" each");

        }else{
            priceTextView.setText("$0");
        }

    }

    public void onCheckboxClickedWhippedCream(View view){
        updateWhippedCreamPrice();
        updateUnitPrice();
        updateOrderSummary();

    }

    public void updateWhippedCreamPrice(){
        String parsedPriceStr = "";
        parsedPriceStr = whippedCream.getText().toString();
        parsedPriceStr = parsedPriceStr.substring(parsedPriceStr.indexOf("$")+1,parsedPriceStr.indexOf(")") );
        int parsedPriceInt = Integer.valueOf(parsedPriceStr);
        whippedCreamPrice = parsedPriceInt;
    }

    public void updateUnitPrice(){
        if(whippedCream.isChecked()){   unitPrice = coffeePrice+whippedCreamPrice;}
        else{                           unitPrice = coffeePrice;}
    }

    public void updateOrderSummary(){
        updatePriceDetails();
        addRemoveWhippedCream();
    }

    public void addRemoveWhippedCream(){
        String orderSummary = priceTextView.getText().toString();
        String whippedCreamAdded = "Whipped cream added to each coffee";
        orderBeingPreparedMsg = coffeeOrderBeingPreparedMsg;

        int whippedCreamAddedIndex = 0;
        try{
            whippedCreamAddedIndex = orderSummary.indexOf(whippedCreamAdded);
        }catch (Exception e){}

        if(whippedCream.isChecked()){
            orderSummary = orderSummary.concat("\n"+whippedCreamAdded);
            orderBeingPreparedMsg = coffeeWhipCreamOrderBeingPreparedMsg;
        }else if(whippedCreamAddedIndex > -1){
            orderSummary = orderSummary.substring(0,whippedCreamAddedIndex);
        }

        priceTextView.setText(orderSummary);
    }
}