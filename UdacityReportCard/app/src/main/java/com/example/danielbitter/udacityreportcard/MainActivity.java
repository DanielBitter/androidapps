package com.example.danielbitter.udacityreportcard;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button play = (Button) findViewById(R.id.btnPlay);
        play.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View view){

//                MediaPlayer mediaPlayer = null;
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sample);
                mediaPlayer.start();
            }
        });

        Button stop = (Button) findViewById(R.id.btnPause);
        stop.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View view){

//                MediaPlayer mediaPlayer = MediaPlayer.
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        });

        TextView output = (TextView) findViewById(R.id.output);
        output.setText("testing the text");
        ReportCard rc1 = new ReportCard(0, "student name", "course name", 40);
//        rc.setStudentId(0);
//        rc.setStudentName("Testing name");
//        rc.setCourseName("Testing course");
//        rc.setStudentGrade(91);
        String printReportCard1 = rc1.toString();
        ReportCard rc2 = new ReportCard(0, "student name", "course name", 100);
        ReportCard rc3 = new ReportCard(0, "student name", "course name", 91);
        ReportCard rc4 = new ReportCard(0, "student name", "course name", 89);
        ReportCard rc5 = new ReportCard(0, "student name", "course name", 81);
        ReportCard rc6 = new ReportCard(0, "student name", "course name", 79);
        ReportCard rc7 = new ReportCard(0, "student name", "course name", 59);
        output.setText(rc1.toString().concat(
                rc2.toString().concat(
                        rc3.toString().concat(
                                rc4.toString().concat(
                                        rc5.toString().concat(
                                                rc6.toString().concat(
                                                        rc7.toString()
                                                )
                                        )
                                )
                        )
                )));
    }



    public void play(View view){
//        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sample);
//        mediaPlayer.start(); // no need to call prepare(); create() does that for you
    }

    public void stop(View view){
//        mediaPlayer.stop();
//        mediaPlayer.release();
//        mediaPlayer = null;
    }
}
