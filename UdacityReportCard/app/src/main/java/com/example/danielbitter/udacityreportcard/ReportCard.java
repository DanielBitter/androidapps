package com.example.danielbitter.udacityreportcard;

import android.util.Log;

/**
 * Created by danielbitter on 12/7/16.
 */

public class ReportCard {
    private int studentGrade;
    private int studentId;
    private String studentName;
    private String courseName;
    private final int[] numGrade =       {60,  70,  80,  90,  100};
    private final String[] letterGrade = {"F", "D", "C", "B", "A"};


    public ReportCard(){

    }

    public ReportCard(int id, String studentNm, String courseNm, int grade){
        this.studentId = id;
        this.studentName = studentNm;
        this.courseName = courseNm;
        this.studentGrade = grade;
    }

    @Override
    public String toString(){
        String output = "";
        output = output.concat("The student "+ getStudentName())
                .concat(" with Student ID: "+ getStudentId())
                .concat(" has earned a grade of : "+ getStudentGrade())
                .concat(" percent in the "+ getCourseName())
                .concat(" course. This translates to a letter grade of "+ getLetterGrade(getStudentGrade()))
                .concat(".\n");
        Log.v("ReportCard",output);
        return output;
    }

    public String getLetterGrade(int num){
        if(-1 < num && num < 101){
            int i=0;
            while(num > numGrade[i] && i < 5){
                ++i;
            }
            return letterGrade[i];
        }else{
            return "Invalid percentage";
        }
    }

    public void setStudentGrade(int grade){
        this.studentGrade = grade;
    }

    public int getStudentGrade(){
        return this.studentGrade;
    }

    public void setStudentId(int id){
        this.studentId = id;
    }

    public int getStudentId(){
        return this.studentId;
    }

    public void setStudentName(String name){
        this.studentName = name;
    }

    public String getStudentName(){
        return this.studentName;
    }

    public void setCourseName(String course){
        this.courseName = course;
    }

    public String getCourseName(){
        return this.courseName;
    }
}
