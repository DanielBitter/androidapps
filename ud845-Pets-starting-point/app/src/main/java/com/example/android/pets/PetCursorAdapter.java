package com.example.android.pets;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.pets.data.PetContract.PetEntry;

/**
 * Created by AH0662542 on 5/1/2017.
 */

public class PetCursorAdapter extends CursorAdapter {
    public PetCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView petName = (TextView) view.findViewById(R.id.pet_name);
        TextView petBreed = (TextView) view.findViewById(R.id.pet_breed);

        String name = "";
        String breed = "";
        String ex = "";

        try{
            name = cursor.getString(cursor.getColumnIndex(PetEntry.COLUMN_PET_NAME));
            breed = cursor.getString(cursor.getColumnIndex(PetEntry.COLUMN_PET_BREED));
        }catch (Exception e){
            ex = e.toString();
        }

        petName.setText(name);
        petBreed.setText(breed);
    }
}
