package com.example.android.pets;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
/**
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.widget.SimpleCursorAdapter;
**/

import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.pets.data.PetContract;
import com.example.android.pets.data.PetContract.PetEntry;

/**
 * Displays list of pets that were entered and stored in the app.
 */
public class CatalogActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {

    SimpleCursorAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        // Setup FAB to open EditorActivity
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CatalogActivity.this, EditorActivity.class);
                startActivity(intent);
            }
        });

        // Find the ListView which will be populated with the pet data
        ListView petListView = (ListView) findViewById(R.id.list);

        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        View emptyView = findViewById(R.id.empty_view);
        petListView.setEmptyView(emptyView);

        petListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int postion, long id) {
                Intent editorIntent = new Intent(CatalogActivity.this, EditorActivity.class);
                editorIntent.setData(ContentUris.withAppendedId(PetEntry.CONTENT_URI, id));
                startActivity(editorIntent);
            }
        });

        mAdapter = new SimpleCursorAdapter(
                getApplicationContext(),
                R.layout.list_item,
                null,
                new String[]{PetEntry.COLUMN_PET_NAME, PetEntry.COLUMN_PET_BREED},
                new int[]{R.id.pet_name, R.id.pet_breed},
                0);
        petListView.setAdapter(mAdapter);

        getSupportLoaderManager().initLoader(0, null, this);
    }

    /**
     * Temporary helper method to display information in the onscreen TextView about the state of
     * the pets database.
     */
    private void displayDatabaseInfo() {
        Cursor cursor = getContentResolver().query(
                PetEntry.CONTENT_URI,
                PetContract.projection, null, null, null);

        ListView petListView = (ListView) findViewById(R.id.list);
        PetCursorAdapter petAdapter = new PetCursorAdapter(this, cursor);
        petListView.setAdapter(petAdapter);
    }

    private void insertDummyPet(){

        ContentValues values = new ContentValues();
        values.put(PetEntry.COLUMN_PET_NAME, getString(R.string.dummyName));
        values.put(PetEntry.COLUMN_PET_BREED, getString(R.string.dummyBreed));
        values.put(PetEntry.COLUMN_PET_GENDER, getResources().getInteger(R.integer.dummyGender));
        values.put(PetEntry.COLUMN_PET_WEIGHT, getResources().getInteger(R.integer.dummyWeight));

        Uri insertedRow = getContentResolver().insert(PetEntry.CONTENT_URI, values);
        long newRowId = ContentUris.parseId(insertedRow);

        if(newRowId > -1){
            Toast.makeText(this, getString(R.string.dummyPetInserted), Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, getString(R.string.insertNewPetError), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
//        displayDatabaseInfo();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_insert_dummy_data:
                insertDummyPet();
                displayDatabaseInfo();
                return true;
            case R.id.action_delete_all_entries:
                deleteAllPets();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch(id){
            case 0:
                return new CursorLoader(
                        getApplicationContext(),
                        PetEntry.CONTENT_URI,
                        PetContract.projection,
                        null,
                        null,
                        null
                );
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) { //changeCursor
        String s = "";
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) { //swapCursor
        String s = "";
        mAdapter.swapCursor(null);
    }

    private void deleteAllPets() {
        int rowId = getContentResolver().delete(
                    PetEntry.CONTENT_URI,
                    null,
                    null);

        if(rowId > 0){
            Toast.makeText(this, getString(R.string.editor_delete_pet_successful), Toast.LENGTH_SHORT).show();
//            finish();
        }else{
            Toast.makeText(this, getString(R.string.editor_delete_pet_failed), Toast.LENGTH_SHORT).show();
        }
    }
}