/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.pets;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.pets.data.PetContract;
import com.example.android.pets.data.PetContract.PetEntry;

/**
 * Allows user to create a new pet or edit an existing one.
 */
public class EditorActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks {

    private EditText mNameEditText;
    private EditText mBreedEditText;
    private EditText mWeightEditText;
    private Spinner mGenderSpinner;
    private int mGender = PetEntry.GENDER_UNKNOWN;
    private boolean EDIT_MODE = false;
    private boolean mPetHasChanged = false;

    Uri incomingUri;
    String name;
    String breed;
    int gender;
    int weight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        EDIT_MODE = false;
        name = "";
        breed = "";
        gender = -1;
        weight = -1;

        // Find all relevant views that we will need to read user input from
        mNameEditText = (EditText) findViewById(R.id.edit_pet_name);
        mBreedEditText = (EditText) findViewById(R.id.edit_pet_breed);
        mWeightEditText = (EditText) findViewById(R.id.edit_pet_weight);
        mGenderSpinner = (Spinner) findViewById(R.id.spinner_gender);

        mNameEditText.setOnTouchListener(mTouchListener);
        mBreedEditText.setOnTouchListener(mTouchListener);
        mWeightEditText.setOnTouchListener(mTouchListener);
        mGenderSpinner.setOnTouchListener(mTouchListener);

        incomingUri = null;
        try{
            Intent intent = getIntent();
            incomingUri = intent.getData();
        }catch(Exception e){

        }

        if(incomingUri != null){
            EDIT_MODE = true;

            getSupportActionBar().setTitle(getString(R.string.editorActivityTitleEditing));
            getSupportLoaderManager().initLoader(1, null, this);

            Toast.makeText(this, "incomingUri: " + incomingUri.toString(), Toast.LENGTH_SHORT).show();
        }else{
            getSupportActionBar().setTitle(getString(R.string.editor_activity_title_new_pet));
            invalidateOptionsMenu();
        }

        setupSpinner();
    }

    /**
     * Setup the dropdown spinner that allows the user to select the gender of the pet.
     */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        mGenderSpinner.setAdapter(genderSpinnerAdapter);

        // Set the integer mSelected to the constant values
        mGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.gender_male))) {
                        mGender = PetEntry.GENDER_MALE; // Male
                    } else if (selection.equals(getString(R.string.gender_female))) {
                        mGender = PetEntry.GENDER_FEMALE; // Female
                    } else {
                        mGender = PetEntry.GENDER_UNKNOWN; // Unknown
                    }
                }
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender = PetEntry.GENDER_UNKNOWN; // Unknown
            }
        });
    }

    private boolean savePet(){
        String saveAttemptMsg = "saveAttemptMsg";
        long rowId;
        String name = "";
        String breed = "";
        int weight = 0;

        try{
            name = mNameEditText.getText().toString().trim();
            breed = mBreedEditText.getText().toString().trim();
            weight = Integer.parseInt(mWeightEditText.getText().toString().trim());
        }catch (Exception e){
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }

        if(     TextUtils.isEmpty(name) ||
                TextUtils.isEmpty(breed)){
            rowId = -1;
        }else{
            ContentValues values = new ContentValues();
            values.put(PetEntry.COLUMN_PET_NAME, name);
            values.put(PetEntry.COLUMN_PET_BREED, breed);
            values.put(PetEntry.COLUMN_PET_GENDER, mGender);
            values.put(PetEntry.COLUMN_PET_WEIGHT, weight);

            if(EDIT_MODE){
                rowId = getContentResolver().update(
                        incomingUri,
                        values,
                        null,
                        null
                );
            }else{
                Uri insertedRow = null;
                insertedRow = getContentResolver().insert(PetEntry.CONTENT_URI, values);
                rowId = ContentUris.parseId(insertedRow);
            }
        }

        Log.v("savePet", "row id: " + rowId);
        saveAttemptMsg = getString(R.string.insertNewPetPropertyError);

        if (rowId > -1) {
            saveAttemptMsg = getString(R.string.insertNewPetSuccess);
            Toast.makeText(this, saveAttemptMsg, Toast.LENGTH_SHORT).show();
            return true;
        }else{
            Toast.makeText(this, saveAttemptMsg, Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                if(savePet()){
                    finish();
                    return true;
                }else{
                    return false;
                }

            case R.id.action_delete:
                if(EDIT_MODE){
                    showDeleteConfirmationDialog();
                }
                return true;
            case android.R.id.home:
                // If the pet hasn't changed, continue with navigating up to parent activity
                // which is the {@link CatalogActivity}.
                if (!mPetHasChanged) {
                    NavUtils.navigateUpFromSameTask(EditorActivity.this);
                    return true;
                }

                // Otherwise if there are unsaved changes, setup a dialog to warn the user.
                // Create a click listener to handle the user confirming that
                // changes should be discarded.
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // User clicked "Discard" button, navigate to parent activity.
                                NavUtils.navigateUpFromSameTask(EditorActivity.this);
                            }
                        };

                // Show a dialog that notifies the user they have unsaved changes
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cL = null;
        switch(id){
        case 1:
//            if(incomingUri != null){
            if(EDIT_MODE){
                cL = new CursorLoader(
                        getApplicationContext(),
                        incomingUri,
                        PetContract.projection,
                        null,
                        null,
                        null
                    );
                return cL;
            }else{
//                return null;
                return cL;
            }
        default:
//            return null;
            return cL;
    }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        Cursor singlePetInfo = null;

        try{
            singlePetInfo = (Cursor) data;
            if (singlePetInfo.moveToFirst()) {
                name = singlePetInfo.getString(singlePetInfo.getColumnIndex(PetEntry.COLUMN_PET_NAME));
                breed = singlePetInfo.getString(singlePetInfo.getColumnIndex(PetEntry.COLUMN_PET_BREED));
                gender = singlePetInfo.getInt(singlePetInfo.getColumnIndex(PetEntry.COLUMN_PET_GENDER));
                weight = singlePetInfo.getInt(singlePetInfo.getColumnIndex(PetEntry.COLUMN_PET_WEIGHT));

                switch (gender) {
                    case PetEntry.GENDER_MALE:
                        mGenderSpinner.setSelection(1);
                        break;
                    case PetEntry.GENDER_FEMALE:
                        mGenderSpinner.setSelection(2);
                        break;
                    default:
                        mGenderSpinner.setSelection(0);
                        break;
                }
            }

            singlePetInfo.close();
        }catch(Exception e){
            String error = e.toString();
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        }

        mNameEditText.setText(name);
        mBreedEditText.setText(breed);
        mWeightEditText.setText(String.valueOf(weight));
    }

    @Override
    public void onLoaderReset(Loader loader) {
        mNameEditText.setText("");
        mBreedEditText.setText("");
        mWeightEditText.setText("");
        mGenderSpinner.setSelection(PetEntry.GENDER_UNKNOWN);
    }

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mPetHasChanged = true;
            return false;
        }
    };

    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Keep editing" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        // If the pet hasn't changed, continue with handling back button press
        if (!mPetHasChanged) {
            super.onBackPressed();
            return;
        }

        // Otherwise if there are unsaved changes, setup a dialog to warn the user.
        // Create a click listener to handle the user confirming that changes should be discarded.
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // User clicked "Discard" button, close the current activity.
                        finish();
                    }
                };

        // Show dialog that there are unsaved changes
        showUnsavedChangesDialog(discardButtonClickListener);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new pet, hide the "Delete" menu item.
        if (!EDIT_MODE) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                deletePet();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the pet in the database.
     */
    private void deletePet() {
        int rowId;
        if(EDIT_MODE){
            rowId = getContentResolver().delete(
                    incomingUri,
                    null,
                    null
            );
        }else{
            rowId = getContentResolver().delete(
                    incomingUri,
                    null,
                    null);
        }

        if(rowId > 0){
            Toast.makeText(this, getString(R.string.editor_delete_pet_successful), Toast.LENGTH_SHORT).show();
            finish();
        }else{
            Toast.makeText(this, getString(R.string.editor_delete_pet_failed), Toast.LENGTH_SHORT).show();
        }
    }
}