Project Name: Basketball Score Counter

Author: Dan Bitter

Description: Udacity course app for teaching User Input. The app is split to two sides for each team in a game of basketball. The user can track the teams’ scores by accounting for free throws, 2-, and 3-point baskets. The scores can be reset to 0, and there is no scoring limit.

Deployment: The application can be loaded to Android Studio and built for an 
emulator or attached device. 

Requirements: Currently targeted for min sdk version 15

Contact/bug reports/feature requests:
bitter.daniel@gmail.com
linkedin.com/in/danielbitter

File list:
/app/manifests/AndroidManifest.xml
/app/java/com.example.android.alpcpart1/MainActivity.java
/app/res/layout/activity_main.xml
/app/res/mipmap/ic_launcher.png
/app/res/values/colors.xml
/app/res/values/dimens.xml
/app/res/values/integers.xml
/app/res/values/strings.xml
/app/res/values/styles.xml