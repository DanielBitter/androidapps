package com.example.android.basketballscorecounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.os.Handler;

public class MainActivity extends AppCompatActivity {
    private Handler mHandler = new Handler();
    private int team1score=0;
    private int team2score=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void teamScore(int team, int pointsToAdd){
        TextView score = (TextView) findViewById(R.id.team1Score);
        if(team == 2){
            score = (TextView) findViewById(R.id.team2Score);
            team2score += pointsToAdd;
        }else{
            team1score += pointsToAdd;
        }

        while(pointsToAdd>0){
            updateScoreboard();
            pointsToAdd--;
        }
    }

    public void updateScoreboard(){
        TextView scoreView = (TextView) findViewById(R.id.team1Score);
        int score = Integer.valueOf(scoreView.getText().toString());
        if(score < team1score){
            score++;
            scoreView.setText(String.valueOf(score));
        }

        TextView scoreView2 = (TextView) findViewById(R.id.team2Score);
        int score2 = Integer.valueOf(scoreView2.getText().toString());
        if(score2 < team2score){
            score2++;
            scoreView2.setText(String.valueOf(score2));
        }

    }

    public void resetGame(View view){
        team1score=0;

        team2score=0;
        TextView score = (TextView) findViewById(R.id.team1Score);
        score.setText(String.valueOf(team1score));

        TextView score2 = (TextView) findViewById(R.id.team2Score);
        score2.setText(String.valueOf(team2score));
    }

    public void team1score3(View view){
        teamScore(1,3);
    }
    public void team1score2(View view){
        teamScore(1,2);
    }
    public void team1score1(View view){
        teamScore(1,1);
    }

    public void team2score3(View view){
        teamScore(2,3);
    }
    public void team2score2(View view){
        teamScore(2,2);
    }
    public void team2score1(View view){
        teamScore(2,1);
    }
}
