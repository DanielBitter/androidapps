package com.example.udacitynewsapp;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Helper methods related to requesting and receiving book data
 */
public final class QueryUtils {
    private static final String LOG_TAG = "QueryUtils";

    public QueryUtils() {
    }

    private static List<Article> extractFeatureFromJson(String jsonData) {

        List<Article> articles = new ArrayList<>();

        try {
            if (jsonData == null || TextUtils.isEmpty(jsonData) || jsonData.length() < 1) {
                return null;
            }

            JSONObject root = new JSONObject(jsonData);
            JSONObject rootFeaturesObj = root.getJSONObject("response");
            JSONArray rootFeaturesArray = rootFeaturesObj.getJSONArray("results");
            SimpleDateFormat sdfOrig = new SimpleDateFormat("yyyy-MM-dd");

            for (int i=0;i<rootFeaturesArray.length();i++) {
                JSONObject articleIdx = rootFeaturesArray.getJSONObject(i);

                String id = articleIdx.getString("id");
                String type = articleIdx.getString("type");
                String sectionId = articleIdx.getString("sectionId");
                String sectionName = articleIdx.getString("sectionName");
                String webPublicationDateString = articleIdx.getString("webPublicationDate");
                String webTitle = articleIdx.getString("webTitle");
                String webUrlString = articleIdx.getString("webUrl");
                String apiUrlString = articleIdx.getString("apiUrl");
                Boolean isHosted = articleIdx.getBoolean("isHosted");

                Date webPublicationDate = null;
                URL webUrl = null;
                URL apiUrl = null;

                try {
                    webPublicationDate = sdfOrig.parse(webPublicationDateString.substring(0,10));
                    webUrl = new URL(webUrlString);
                    apiUrl = new URL(apiUrlString);
                }catch (ParseException p){
                    Log.v(LOG_TAG, p.toString());
                }catch (MalformedURLException m){
                    Log.v(LOG_TAG, m.toString());
                }

                Article a = new Article();
                a.setTitle(id);
                a.setType(type);
                a.setSectionId(sectionId);
                a.setSectionName(sectionName);
                a.setWebPublicationDate(webPublicationDate);
                a.setTitle(webTitle);
                a.setWebUrl(webUrl);
                a.setApiUrl(apiUrl);
                a.setIsHosted(isHosted);

                articles.add(a);
                Log.v(LOG_TAG,"Added article: " + id);
            }

        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the JSON results", e);
        }

        return articles;
    }

    public static List<Article> fetchArticleData(String requestUrl) {
//        try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();} //show loading indicator

        Log.v(LOG_TAG,"fetchArticleData()");
        // Create URL object
        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making the HTTP request.", e);
            return null;
        }

        // Extract relevant fields from the JSON response and create a list of {@link Earthquake}s
        List<Article> articles = extractFeatureFromJson(jsonResponse);

        return articles;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        // If the URL is null, then return early.
        if (url == null || url.toString().length() < 1) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

}