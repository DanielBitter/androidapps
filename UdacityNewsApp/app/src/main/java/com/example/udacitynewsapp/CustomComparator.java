package com.example.udacitynewsapp;

import java.util.Comparator;

/**
 * Created by AH0662542 on 4/24/2017.
 * http://stackoverflow.com/questions/2784514/sort-arraylist-of-custom-objects-by-property
 */

public class CustomComparator {

}

class CustomComparatorDateAsc implements Comparator<Article> {
    @Override
    public int compare(Article o1, Article o2) {
        return o1.getWebPublicationDate().compareTo(o2.getWebPublicationDate());
    }
}

class CustomComparatorDateDesc implements Comparator<Article> {
    @Override
    public int compare(Article o1, Article o2) {
        int result = o1.getWebPublicationDate().compareTo(o2.getWebPublicationDate());
        result *= -1;
        return result;
    }
}

class CustomComparatorTitleAsc implements Comparator<Article> {
    @Override
    public int compare(Article o1, Article o2) {
        return o1.getTitle().compareTo(o2.getTitle());
    }
}

class CustomComparatorTitleDesc implements Comparator<Article> {
    @Override
    public int compare(Article o1, Article o2) {
        int result = o1.getTitle().compareTo(o2.getTitle()) * -1;
        result *= -1;
        return result;
    }
}