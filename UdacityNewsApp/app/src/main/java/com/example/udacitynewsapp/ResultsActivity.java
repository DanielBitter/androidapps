/**
 * https://www.caveofprogramming.com/guest-posts/custom-listview-with-imageview-and-textview-in-android.html
 * http://stackoverflow.com/questions/11092599/how-can-i-refresh-the-cursor-from-a-cursorloader
 * https://developer.android.com/training/appbar/actions.html#handle-actions
 * https://developer.android.com/guide/topics/ui/dialogs.html
 */
package com.example.udacitynewsapp;

import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultsActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<Article>>
{
    public RelativeLayout layoutResults;

    private ArticleAdapter mAdapter;
    private TextView mEmptyStateTextView;
    private String intentParamSearchTerm = "";
    private static final String ARTICLE_REQUEST_URL =
            "http://content.guardianapis.com/search?q=";
    private static final int ARTICLE_LOADER_ID = 1;
    private static String searchParam = "";
    private List<Article> privateData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        searchParam = getApplicationContext().getString(R.string.intentSearchParamKey);

        Intent intent = getIntent();
        intentParamSearchTerm = intent.getStringExtra(searchParam);
        intentParamSearchTerm = ARTICLE_REQUEST_URL
                .concat(intentParamSearchTerm)
                .concat("&api-key=")
                .concat(getString(R.string.api_key));

        mEmptyStateTextView = (TextView) findViewById(R.id.textViewEmpty);
        ListView articleListView = (ListView) findViewById(R.id.resultsListView);
        articleListView.setEmptyView(mEmptyStateTextView);

        mAdapter = new ArticleAdapter(this, new ArrayList<Article>());

        articleListView.setAdapter(mAdapter);
        articleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String openingUrl = getApplicationContext().getString(R.string.urlOpening);
                URL url = mAdapter.getItem(position).getWebUrl();
                if(url.toString().length() < 1){
                    openingUrl = getApplicationContext().getString(R.string.errorNoUrl);
                }
                Toast.makeText(getApplicationContext(), openingUrl, Toast.LENGTH_SHORT).show();

                Uri webpage = Uri.parse(url.toString());
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(ARTICLE_LOADER_ID, null, this);
    }


    private boolean isNetworkConnected(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isConnected = false;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        Toast.makeText(this, String.valueOf(isConnected), Toast.LENGTH_SHORT).show();
        return isConnected;
    }

    @Override
    public Loader<List<Article>> onCreateLoader(int id, Bundle args) {
        return new ArticleLoader(this, intentParamSearchTerm);
    }

    @Override
    public void onLoadFinished(Loader<List<Article>> loader, List<Article> data) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        mAdapter.clear();
        if (data != null && !data.isEmpty()) {
            populateData(data);
        }else{
            mEmptyStateTextView.setVisibility(View.VISIBLE);
            mEmptyStateTextView.setText(getString(R.string.noBooksFound));

            if(!isNetworkConnected()){
                mEmptyStateTextView.setText(getString(R.string.noNetworkConnectivity));
            }
        }
        layoutResults = (RelativeLayout) findViewById(R.id.layoutResults);
        layoutResults.setVisibility(View.VISIBLE);
    }

    private void populateData(List<Article> incomingData){
        this.privateData = incomingData;

        sortPrivateData("date","desc");
        mEmptyStateTextView.setVisibility(View.GONE);
        mAdapter.addAll(this.privateData);
    }

    private void sortPrivateData(String parm, String direction){
        switch (parm){
            case "date":
                if(direction.equalsIgnoreCase("asc")){
                    Collections.sort(this.privateData, new CustomComparatorDateAsc());
                }else{
                    Collections.sort(this.privateData, new CustomComparatorDateDesc());
                }
                break;
            case "title":
                if(direction.equalsIgnoreCase("asc")){
                    Collections.sort(this.privateData, new CustomComparatorTitleAsc());
                }else{
                    Collections.sort(this.privateData, new CustomComparatorTitleDesc());
                }
                break;
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<List<Article>> loader) {
        mEmptyStateTextView.setText("");
        mEmptyStateTextView.setVisibility(View.GONE);
        mAdapter.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.actionbar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
//                Toast.makeText(this,"sorting", Toast.LENGTH_SHORT).show();
                showRadioButtonDialog();
                return true;

            case R.id.action_refresh:
//                Toast.makeText(this,"refreshing", Toast.LENGTH_SHORT).show();
                getLoaderManager().restartLoader(ARTICLE_LOADER_ID, null, this);
                return true;

            default:
                Toast.makeText(this,"refreshing", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);

        }
    }

    private void showRadioButtonDialog() {
        String[] sortOptions = {
                getString(R.string.sortNewTop),
                getString(R.string.sortNewBottom),
                getString(R.string.sortTitleAZ),
                getString(R.string.sortTitleZA)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.sortMethod)
                .setItems(sortOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int selection) {
                        switch (selection){
                            default:
                            case 0:
                                sortPrivateData("date","desc");
                                break;
                            case 1:
                                sortPrivateData("date","asc");
                                break;
                            case 2:
                                sortPrivateData("title","desc");
                                break;
                            case 3:
                                sortPrivateData("title","asc");
                                break;
                        }
                    }
                });
        builder.create();
        builder.show();
    }
}