package com.example.udacitynewsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * https://www.sitepoint.com/custom-data-layouts-with-your-own-android-arrayadapter/
 */

public class ArticleAdapter extends ArrayAdapter<Article> {
    private final Context context;
    private final List<Article> articleData;

    public ArticleAdapter(Context ct, List<Article> eqs){
        super(ct, 0, eqs);
        this.context = ct;
        this.articleData = eqs;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        String title = articleData.get(position).getTitle();
        String section = articleData.get(position).getSectionName();
        Date date = articleData.get(position).getWebPublicationDate();

        SimpleDateFormat sdfNew = new SimpleDateFormat("MM/dd/yyyy");
        String webPublicationDateString = sdfNew.format(date);

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_result_item, null);
        }

        TextView titleTv = (TextView) convertView.findViewById(R.id.textViewResultTitle);
        TextView dateTv = (TextView) convertView.findViewById(R.id.textViewResultSectionDate);

        titleTv.setText(title);
        dateTv.setText(section.concat(", ").concat(webPublicationDateString));
        return convertView;
    }
}