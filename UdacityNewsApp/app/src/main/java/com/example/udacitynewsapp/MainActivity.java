package com.example.udacitynewsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private String searchParam = "";
    private String inputError = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputError = this.getString(R.string.errorValidInput);
        searchParam = getString(R.string.intentSearchParamKey);
    }

    public void startLoaderProcess(View view) {
        doSearch();
    }

    public void doSearch() {
        Intent intent = new Intent(this, ResultsActivity.class);
        EditText input = (EditText) findViewById(R.id.editTextInputBox);
        String inputSearchTerm = input.getText().toString();
        inputSearchTerm = formatSearch(inputSearchTerm);
        if (searchParam != null || !searchParam.isEmpty()) {
            intent.putExtra(searchParam, inputSearchTerm);
            try {
                startActivity(intent);
            } catch (Exception e) {
                Log.v("doSearch", e.toString());
            }
        } else {
            Toast.makeText(this, inputError, Toast.LENGTH_SHORT).show();
        }
    }

    public String formatSearch(String input) {
        input = input.replaceAll("[^A-Za-z0-9 ]", "");
        input = input.trim();
        input = input.replaceAll(" ", "%20");
        return input;
    }
}
