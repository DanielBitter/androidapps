package com.example.udacitynewsapp;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

public class ArticleLoader
        extends AsyncTaskLoader<List<Article>> {
    private String url;

    public ArticleLoader(Context context, String urlArg) {
        super(context);
        this.url = urlArg;
    }

    @Override
    public List<Article> loadInBackground() {
        if(url == null || url.length() < 1){
            return null;
        }
        List<Article> articleData = QueryUtils.fetchArticleData(url);
        return articleData;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
