package com.example.udacitynewsapp;

import java.net.URL;
import java.util.Date;

public class Article {

    private String id;
    private String type;
    private String sectionId;
    private String sectionName;
    private Date webPublicationDate;
    private String title;
    private URL webUrl;
    private URL apiUrl;
    private Boolean isHosted;

    public void setId(String s){
        this.id = s;
    }
    public void setType(String t){
        this.type = t;
    }
    public void setSectionId(String s){this.sectionId = s;}
    public void setSectionName(String s){
        this.sectionName = s;
    }
    public void setWebPublicationDate(Date d){this.webPublicationDate = d;}
    public void setTitle(String t){
        this.title = t;
    }
    public void setWebUrl(URL u){this.webUrl = u;}
    public void setApiUrl(URL u){this.apiUrl = u;}
    public void setIsHosted(Boolean b){this.isHosted = b;}

    public String getId(){
        return this.id;
    }
    public String getType(){return this.type;}
    public String getSectionId(){return this.sectionId;}
    public String getSectionName(){
        return this.sectionName;
    }
    public Date getWebPublicationDate(){return this.webPublicationDate;}
    public String getTitle(){
        return this.title;
    }
    public URL getWebUrl(){return this.webUrl;}
    public URL getApiUrl(){return this.apiUrl;}
    public Boolean getIsHosted(){return this.isHosted;}
}
