package com.example.inventorytracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.inventorytracker.data.InventoryContract.InventoryEntry;

/**
 * Created by danielbitter on 6/1/17.
 * https://stackoverflow.com/questions/11160639/list-item-with-button-not-clickable-anymore
 */

public class EntryAdapter extends CursorAdapter {
    private final Context context;
    private final Cursor cursor;

    public EntryAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        this.context = context;
        this.cursor = cursor;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_inventory, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        TextView tv_item_name = (TextView) view.findViewById(R.id.tv_item_name);
        TextView tv_item_quantity = (TextView) view.findViewById(R.id.tv_item_quantity);
        TextView tv_item_price = (TextView) view.findViewById(R.id.tv_item_price);

        final String str_name = cursor.getString(cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_NAME));
        tv_item_name.setText(str_name);

        String quantity = cursor.getString(cursor.getColumnIndex(
                InventoryEntry.COLUMN_ENTRY_QUANTITY));
        final String str_quantityFinal = quantity;
        String regex = "(\\d)(?=(\\d{3})+$)";
        quantity = quantity.replaceAll(regex, "$1,"); //"$1,\n"); //awkward for 1,000
        //todo figure a better way to display , , , in quantities
        tv_item_quantity.setText(quantity);

        final String priceDollarsCents = convertPriceToDollarsCents(cursor.getString(
                cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_PRICE)));
        tv_item_price.setText(priceDollarsCents);

        final int int_updatedSalesInt = Integer.parseInt(cursor.getString(cursor.getColumnIndex(
                InventoryEntry.COLUMN_ENTRY_SALES_TOTAL)));
        final Uri uri_listItem = Uri.parse(
                cursor.getString(cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_ITM_URI)));
        final int int_price = cursor.getInt(cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_PRICE));
        final String str_imgUri = cursor.getString(cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_IMG_URI));

        Button btn_rcdAsale = (Button)view.findViewById(R.id.btn_rcdAsale);btn_rcdAsale.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ContentValues values = new ContentValues();
                values.put(InventoryEntry.COLUMN_ENTRY_NAME, str_name);

                int int_updatedSales = int_updatedSalesInt;
                String str_quantity = str_quantityFinal;
                if(str_quantityFinal.length() > 0){
                    int int_potentialChange = Integer.parseInt(str_quantityFinal)-1;
                    if(int_potentialChange > -1){
                        str_quantity = String.valueOf(int_potentialChange);
                        int_updatedSales += 1;
                    }
                }
                String str_updatedSales = String.valueOf(int_updatedSales);

                values.put(InventoryEntry.COLUMN_ENTRY_QUANTITY, str_quantity);
                values.put(InventoryEntry.COLUMN_ENTRY_PRICE, int_price);
                values.put(InventoryEntry.COLUMN_ENTRY_IMG_URI, str_imgUri);
                values.put(InventoryEntry.COLUMN_ENTRY_SALES_TOTAL, str_updatedSales);
                final int rowsUpdated = context.getContentResolver().
                        update(uri_listItem, values, null, null);
                notifyDataSetChanged();
            }
        });
    }

    public static String convertPriceToDollarsCents(String priceStoredAsCents){
        double d = Double.valueOf(priceStoredAsCents);
        String dollarsCentsString = String.format( "%.2f", d/100.00);

        return dollarsCentsString;
    }
}
