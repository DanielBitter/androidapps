package com.example.inventorytracker;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.inventorytracker.data.InventoryContract;
import com.example.inventorytracker.data.InventoryContract.InventoryEntry;

public class EntryListMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_list_main);

        P.passContextForOutputEngine(this);

        ListView inventoryList = (ListView) findViewById(R.id.lv_inventoryList);
        inventoryList.setEmptyView(findViewById(R.id.tv_emptyView));

        AdapterView.OnItemClickListener click_listItem = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), EntryDetailActivity.class);
                intent.setData(Uri.withAppendedPath(InventoryEntry.CONTENT_URI, String.valueOf(id)));
                startActivity(intent);
            }
        };
        inventoryList.setOnItemClickListener(click_listItem);

        Cursor cursorQuery = getContentResolver().query(
                InventoryEntry.CONTENT_URI,
                InventoryContract.projection,
                null,
                null,
                null);

        EntryAdapter entryAdapter = new EntryAdapter(this, cursorQuery);
        inventoryList.setAdapter(entryAdapter);

        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.fab_addEntry);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EntryDetailActivity.class);
                startActivity(intent);
            }
        });
    }
}
