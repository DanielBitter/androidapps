package com.example.inventorytracker.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.inventorytracker.data.InventoryContract.InventoryEntry;


public class InventoryDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Inventory.db";
    public InventoryDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_PETS_TABLE =  "CREATE TABLE " + InventoryEntry.TABLE_NAME + " (" +
                InventoryEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                InventoryEntry.COLUMN_ENTRY_NAME + " TEXT NOT NULL, " +
                InventoryEntry.COLUMN_ENTRY_QUANTITY + " INTEGER NOT NULL DEFAULT 0, "+
                InventoryEntry.COLUMN_ENTRY_PRICE + " INTEGER NOT NULL DEFAULT 0, "+
                InventoryEntry.COLUMN_ENTRY_SALES_TOTAL + " INTEGER NOT NULL DEFAULT 0, " +
                InventoryEntry.COLUMN_ENTRY_IMG_URI + " TEXT NOT NULL, " +
                InventoryEntry.COLUMN_ENTRY_ITM_URI + " TEXT NOT NULL); ";
        db.execSQL(SQL_CREATE_PETS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
