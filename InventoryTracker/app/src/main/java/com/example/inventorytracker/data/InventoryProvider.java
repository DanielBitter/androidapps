package com.example.inventorytracker.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.inventorytracker.P;
import com.example.inventorytracker.R;
import com.example.inventorytracker.data.InventoryContract.InventoryEntry;

public class InventoryProvider extends ContentProvider {

    private final String LOG_TAG = this.getClass().getSimpleName();
    private static final int id_inventory = 0;
    private static final int id_entry = 1;

    private InventoryDbHelper iDbHelper;
    private static final UriMatcher uriMatch = new UriMatcher(UriMatcher.NO_MATCH);

    static{
        uriMatch.addURI(InventoryContract.CONTENT_AUTHORITY,
                InventoryContract.PATH_INVENTORY, id_inventory);
        uriMatch.addURI(InventoryContract.CONTENT_AUTHORITY,
                InventoryContract.PATH_INVENTORY + "/#", id_entry);
    }

    @Override
    public boolean onCreate() {
        iDbHelper = new InventoryDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = iDbHelper.getReadableDatabase();
        Cursor cursor = null;

        switch(uriMatch.match(uri)){
            case id_entry:
                selection = "_id=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
            case id_inventory:
                cursor = db.query(
                        InventoryEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.error_unrecogUri));
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = uriMatch.match(uri);
        switch (match) {
            case id_inventory:
                return InventoryEntry.CONTENT_LIST_TYPE;
            case id_entry:
                return InventoryEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException(getContext().getString(R.string.error_unrecogUri));
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        switch(uriMatch.match(uri)){
            case id_inventory:
                return insertEntry(uri, values);
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.error_unrecogUri));
        }
    }

    private Uri insertEntry(Uri uri, ContentValues values){
        String insertionError = getContext().getString(R.string.error_insertion);
        Uri insertedUri = null;
        SQLiteDatabase db = iDbHelper.getWritableDatabase();
        long id = -1;

        try{
            validateValuesDO(values);
            id = db.insert(InventoryEntry.TABLE_NAME, null, values);
            if(id == -1){
                Log.e(LOG_TAG, insertionError);
                P.p(insertionError.concat(" " + uri.toString()), "debug");
                return null;
            }

            P.p(getContext().getString(R.string.success_insert), "user");

            getContext().getContentResolver().notifyChange(uri, null);
            insertedUri = ContentUris.withAppendedId(uri, id);
        }catch(IllegalArgumentException e){
            P.p(insertionError, "user");
        }

        return insertedUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = iDbHelper.getWritableDatabase();
        int numRowsDeleted = 0;

        switch(uriMatch.match(uri)){
            case id_entry:
                selection = InventoryEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
            case id_inventory:
                numRowsDeleted = db.delete(InventoryEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.error_unrecogUri));
        }
        if(numRowsDeleted > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return numRowsDeleted;

    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        switch(uriMatch.match(uri)){
            case id_entry:
                return updateEntry(uri, values, selection, selectionArgs);
            case id_inventory:
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.error_unrecogUri));
        }
    }

    private int updateEntry(Uri uri, ContentValues values, String selection, String[] selectionArgs){
        int rowsUpdated = 0;
        String str_result = getContext().getString(R.string.success_update);

        if(validateValuesDO(values)){
            SQLiteDatabase db = iDbHelper.getWritableDatabase();

            long id = ContentUris.parseId(uri);

            selection = InventoryEntry._ID +"=?";
            selectionArgs = new String[]{String.valueOf(id)};

            rowsUpdated = db.update(InventoryEntry.TABLE_NAME, values, selection, selectionArgs);
            if (rowsUpdated > 0) {
                getContext().getContentResolver().notifyChange(uri, null);
            }else{
                str_result = getContext().getString(R.string.error_update);
            }
        }
        P.p(str_result, "user");

        return rowsUpdated;
    }

    private boolean validateValuesDO(ContentValues values){
        boolean isValid = true;

        String name = values.getAsString(InventoryEntry.COLUMN_ENTRY_NAME);
        int quantity = values.getAsInteger(InventoryEntry.COLUMN_ENTRY_QUANTITY);
        int price = values.getAsInteger(InventoryEntry.COLUMN_ENTRY_PRICE);
        int sales = values.getAsInteger(InventoryEntry.COLUMN_ENTRY_SALES_TOTAL);

        boolean validName = name.length() > 0;
        boolean validQuantity = quantity >= 0;
        boolean validPrice = price >= 0;
        boolean validSales = sales > -1;

        if(!validName){
            isValid = false;
            throw new IllegalArgumentException(getContext().getString(R.string.error_invalidName));
        }
        if(!validQuantity){
            isValid = false;
            throw new IllegalArgumentException(getContext().getString(R.string.error_invalidQuantity));
        }
        if(!validPrice){
            isValid = false;
            throw new IllegalArgumentException(getContext().getString(R.string.error_invalidPrice));
        }
        if(!validSales){
            isValid = false;
            throw new IllegalArgumentException(getContext().getString(R.string.error_invalidSales));
        }

        return isValid;
    }
}
