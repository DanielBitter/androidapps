package com.example.inventorytracker;
//https://stackoverflow.com/questions/5057960/is-there-a-general-string-substitution-function-similar-to-sl4fj
//https://developer.android.com/training/basics/data-storage/files.html#WriteInternalStorage
//https://stackoverflow.com/questions/12339673/fileoutputstream-crashes-with-open-failed-eisdir-is-a-directory-error-when

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.inventorytracker.data.InventoryContract;
import com.example.inventorytracker.data.InventoryContract.InventoryEntry;

public class EntryDetailActivity extends AppCompatActivity {
    EditText et_name;
    EditText et_quantity;
    EditText et_price;
    EditText et_sales;
    TextView tv_sales_total;
    EditText et_shipment;
    ImageView iv_entryPicture;

    Boolean bol_EditMode = false;
    Uri uri_editItem;
    Uri uri_selectedImage;
    String initialQuantity;

    private static final int ORDER_MORE = 1;
    private static final int INV_SALE = 2;
    private static final int INV_RECV = 3;
    private static final int CAMERA_PICTURE = 226; //"cam"
    private static final int SELECT_PICTURE = 742; //"pic"


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_detail);

        et_name = (EditText) findViewById(R.id.et_name);
        et_quantity = (EditText) findViewById(R.id.et_quantity);
        et_price = (EditText) findViewById(R.id.et_price);
        et_sales = (EditText) findViewById(R.id.et_sales);
        tv_sales_total = (TextView) findViewById(R.id.tv_sales_total_value);
        et_shipment = (EditText) findViewById(R.id.et_shipment);
        iv_entryPicture = (ImageView) findViewById(R.id.iv_chooseEntryImage);
        uri_selectedImage = Uri.parse("");
        initialQuantity = getString(R.string.defaultInventoryValue);

        Intent intent = getIntent();
        if(intent.getData() != null){
            bol_EditMode = true;
            uri_editItem = intent.getData();

            Cursor cursor = getContentResolver().query(
                    InventoryEntry.CONTENT_URI,
                    InventoryContract.projection,
                    InventoryEntry._ID + "=?",
                    new String[]{String.valueOf(ContentUris.parseId(uri_editItem))},
                    null);
            cursor.moveToFirst();

            et_name.setText(cursor.getString(
                    cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_NAME)));

            initialQuantity = cursor.getString(
                    cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_QUANTITY));
            et_quantity.setText(initialQuantity);

            String str_dollarsCents = cursor.getString(
                    cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_PRICE));
            str_dollarsCents = EntryAdapter.convertPriceToDollarsCents(str_dollarsCents);
            et_price.setText(str_dollarsCents);

            tv_sales_total.setText(cursor.getString(
                    cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_SALES_TOTAL)));

            String str_ImgUriVal = cursor.getString(cursor.getColumnIndex(InventoryEntry.COLUMN_ENTRY_IMG_URI));
            if(str_ImgUriVal != null){
                uri_selectedImage = Uri.parse(str_ImgUriVal);
                if(!str_ImgUriVal.isEmpty() && uri_selectedImage!= null){
                    setImageViewSource(uri_selectedImage, iv_entryPicture);
                }
            }
            cursor.close();
        }
        String str_selectedImageId = "";
        if(uri_selectedImage.toString().length() > 57){
            str_selectedImageId = uri_selectedImage.toString().substring(57);
        }

        registerForContextMenu(iv_entryPicture);
        iv_entryPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContextMenu(v);
            }
        });
    }

    private void startImageChooser(int mode){
        //https://stackoverflow.com/questions/19837358/android-kitkat-securityexception-when-trying-to-read-from-mediastore
        Intent intent_imageAction = new Intent();
        intent_imageAction.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        switch(mode){
            case 1://choose
                intent_imageAction.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent_imageAction.setType("image/*");
                startActivityForResult(intent_imageAction, SELECT_PICTURE);
                break;
            case 0://take
//                intent_imageAction = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent_imageAction, CAMERA_PICTURE);
                break;
            case -1://remove
                setImageViewSource(Uri.parse(""), iv_entryPicture);
                break;
            default:
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //https://stackoverflow.com/questions/3879992/how-to-get-bitmap-from-an-uri
        //https://stackoverflow.com/questions/5309190/android-pick-images-from-gallery
        //https://stackoverflow.com/questions/2708128/single-intent-to-let-user-take-picture-or-pick-image-from-gallery-in-android
        super.onActivityResult(requestCode, resultCode, data);

        Uri uri_intentResult = null;
        if(data != null){
            uri_intentResult = data.getData();
        }

        if (requestCode == SELECT_PICTURE &&
                resultCode == Activity.RESULT_OK &&
                uri_intentResult !=null) {
            setImageViewSource(uri_intentResult, iv_entryPicture);

        }else if (requestCode == CAMERA_PICTURE &&
                resultCode == Activity.RESULT_OK &&
                uri_intentResult !=null) {
                Uri imageUri = data.getData();
                setImageViewSource(imageUri, iv_entryPicture);
        }
    }

    private boolean setImageViewSource(Uri uri, ImageView iv){
        boolean bol_worked = false;

        String str_result = getString(R.string.error_defaultResult);
        if(uri == null || uri.toString().length() < 1){
            uri = getUriToDrawable(R.drawable.ic_photo_camera_black_24dp);
            str_result = getString(R.string.error_emptyUri);
            P.p(str_result,"user");
        }
        iv.setImageURI(uri);
        uri_selectedImage = uri;

        return bol_worked;
    }

    public final Uri getUriToDrawable(int drawableId) {
        Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + getResources().getResourcePackageName(drawableId)
                + '/' + getResources().getResourceTypeName(drawableId)
                + '/' + getResources().getResourceEntryName(drawableId) );
        return imageUri;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_entry_detail, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_float_image_edit, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_float_take_picture:
                startImageChooser(0);
                return true;
            case R.id.menu_float_choose_picture:
                startImageChooser(1);
                return true;
            case R.id.menu_float_remove_picture:
                startImageChooser(-1);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_detail_save:
                saveEntry();
                return true;
            case R.id.menu_detail_delete:
                AlertDialog diaBox = AskOption("delete");
                diaBox.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private AlertDialog AskOption(String btnPressed)
    {
        AlertDialog alert_confirmDelete = null;
        switch(btnPressed){
            case "back":
                alert_confirmDelete =new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.menu_detail_discard))
                        .setMessage(getString(R.string.dialog_discard))
                        .setPositiveButton(getString(R.string.menu_detail_discard), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int choice) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.menu_detail_cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int choice) {
                                dialog.dismiss();
                            }
                        }).create();
                break;
            case "delete":
                alert_confirmDelete =new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.menu_detail_delete))
                        .setMessage(getString(R.string.dialog_delete))
                        .setPositiveButton(getString(R.string.menu_detail_delete), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int choice) {
                                deleteEntry();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(getString(R.string.menu_detail_cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int choice) {
                                dialog.dismiss();
                            }
                        }).create();
                break;
        }
        return alert_confirmDelete;

    }

    private void saveEntry(){
        String name;
        int quantity;
        int price;
        int sales;
        int sales_total;
        int shipment;
        String result = "results";
        String result_debug = "results";

        try{
            name = et_name.getText().toString();
            if(name.length() < 1) {
                throw new Exception(getString(R.string.error_invalidName));
            }
            quantity = Integer.parseInt(et_quantity.getText().toString());
            quantity = (quantity < 0) ? quantity * -1 : quantity;

            String str_sales = et_sales.getText().toString();
            sales = (str_sales.length() > 0) ? Integer.parseInt(str_sales) : 0;
            quantity -= sales;

            String str_salesTotal = tv_sales_total.getText().toString();
            sales_total = (str_salesTotal.length() > 0) ?
                    Integer.parseInt(str_salesTotal) : 0;

            int quantityChange = Integer.parseInt(initialQuantity) - quantity;
            if(quantityChange > 0){sales = quantityChange;}
            sales_total += sales;

            price = convertPriceToCents(et_price.getText().toString());
            price = (price < 0) ? price * -1 : price;

            result_debug = getString(R.string.values_insertOrUpdate) +
                    name + ", " + quantity + ", " + price + ", " + sales_total;
            String str_shipment = et_shipment.getText().toString();
            shipment = (str_shipment.length() > 0) ? Integer.parseInt(str_shipment) : 0;
            quantity += shipment;

            ContentValues values = new ContentValues();
            values.put(InventoryEntry.COLUMN_ENTRY_NAME, name);
            values.put(InventoryEntry.COLUMN_ENTRY_PRICE, price);
            values.put(InventoryEntry.COLUMN_ENTRY_QUANTITY, quantity);
            values.put(InventoryEntry.COLUMN_ENTRY_SALES_TOTAL, sales_total);

            String selectedImageStr = uri_selectedImage.toString();
            selectedImageStr = (!(selectedImageStr == null) &&
                    selectedImageStr.length() > 0) ? (selectedImageStr) : ("");

            values.put(InventoryEntry.COLUMN_ENTRY_IMG_URI, selectedImageStr);
            values.put(InventoryEntry.COLUMN_ENTRY_ITM_URI, "");

            if(!bol_EditMode){
                Uri newUri = getContentResolver().insert(InventoryEntry.CONTENT_URI, values);
                values.put(InventoryEntry.COLUMN_ENTRY_ITM_URI, newUri.toString());
                int rowsUpdated = getContentResolver().update(newUri, values, null, null);
                result_debug = result.concat(", " + rowsUpdated);
            }else{
                values.put(InventoryEntry.COLUMN_ENTRY_ITM_URI, uri_editItem.toString());
                int rowsUpdated = getContentResolver().update(uri_editItem, values, null, null);
                result_debug = String.valueOf(rowsUpdated)
                        .concat(getString(R.string.success_update_rows))
                        .concat(result);
            }

            finish();
        }catch(NumberFormatException e){ //invalid int or double
            if(e.toString().contains("int")){
                result = getString(R.string.error_invalidQuantity);
            }else if(e.toString().contains("double")){
                result = getString(R.string.error_invalidPrice);
            }
        }catch(Exception e){
            result_debug = e.toString();
            if(result_debug.contains(getString(R.string.error_invalidName))){
                result = (result_debug.length() > 21) ? result_debug.substring(21) : result_debug;
            }
        }

        P.p(result_debug,"debug");
        P.p(result,"user");

    }

    private int convertPriceToCents(String str_userInputPrice){
        int cents;

        String comma = getString(R.string.priceSeparator_comma);
        String period = getString(R.string.priceSeparator_period);
        if(str_userInputPrice.contains(comma)){

            str_userInputPrice = str_userInputPrice.replaceAll(comma, period);
        }
        double dollarsCents = Double.parseDouble(str_userInputPrice) * 100.00;
        cents = (int) dollarsCents;

        return cents;
    }

    private void deleteEntry(){
        if(bol_EditMode){
            getContentResolver().delete(uri_editItem, null, null);
        }
        finish();
    }

    public void orderMore(){
        String name = et_name.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.orderMore_emailAddress)});
        intent.putExtra(Intent.EXTRA_SUBJECT,
                String.format(getString(R.string.orderMore_emailSubject), name));
        intent.putExtra(Intent.EXTRA_TEXT,
                String.format(getString(R.string.orderMore_emailBody), name));
        startActivity(Intent.createChooser(intent, getString(R.string.orderMore_intentTitle)));
    }

    @Override
    public void onBackPressed() {
        AlertDialog diaBox = AskOption("back");
        diaBox.show();
    }

    public void updateInventory(View view){
        String tag = view.getTag().toString();
        P.p(tag, "debug");

        int tagCode = Integer.parseInt(tag);

        switch(tagCode){
            case ORDER_MORE:
                orderMore();
                break;
            case INV_SALE:
                adjustQuantity(-1);
                break;
            case INV_RECV:
                adjustQuantity(1);
                break;
            default:
                P.p(getString(R.string.error_unrecogBtn), "debug");
                break;
        }
    }

    private void adjustQuantity(int change){
        String existingText = "";
        int currentQuantity = 0;
        try{
            existingText = et_quantity.getText().toString();
            currentQuantity = Integer.parseInt(existingText);
        }catch(Exception e){
            P.p(e.toString(), getString(R.string.error_invalidQuantity));
        }

        currentQuantity = ((currentQuantity + change) < 0) ? 0 : (currentQuantity + change);

        et_quantity.setText(String.valueOf(currentQuantity));

    }

}
