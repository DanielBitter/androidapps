package com.example.inventorytracker.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class InventoryContract {

    public static final String CONTENT_AUTHORITY = "com.example.inventorytracker";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_INVENTORY = "inventory";
    public static String[] projection = {
            InventoryEntry._ID,
            InventoryEntry.COLUMN_ENTRY_NAME,
            InventoryEntry.COLUMN_ENTRY_QUANTITY,
            InventoryEntry.COLUMN_ENTRY_PRICE,
            InventoryEntry.COLUMN_ENTRY_SALES_TOTAL,
            InventoryEntry.COLUMN_ENTRY_IMG_URI,
            InventoryEntry.COLUMN_ENTRY_ITM_URI};

    private InventoryContract() {}

    public static class InventoryEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_INVENTORY);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INVENTORY;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INVENTORY;

        public static final String TABLE_NAME = "CurrentInventory";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ENTRY_NAME = "name";
        public static final String COLUMN_ENTRY_QUANTITY = "quantity";
        public static final String COLUMN_ENTRY_PRICE = "price";
        public static final String COLUMN_ENTRY_SALES_TOTAL = "totalSales";
        public static final String COLUMN_ENTRY_IMG_URI = "imgUri";
        public static final String COLUMN_ENTRY_ITM_URI = "itemUri";

        private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + InventoryEntry.TABLE_NAME + " (" +
                        InventoryEntry._ID + " INTEGER PRIMARY KEY," +
                        InventoryEntry.COLUMN_ENTRY_NAME + " TEXT," +
                        InventoryEntry.COLUMN_ENTRY_QUANTITY + " INTEGER," +
                        InventoryEntry.COLUMN_ENTRY_PRICE + " INTEGER," +
                        InventoryEntry.COLUMN_ENTRY_SALES_TOTAL + " INTEGER," +
                        InventoryEntry.COLUMN_ENTRY_IMG_URI + " TEXT," +
                        InventoryEntry.COLUMN_ENTRY_ITM_URI + " TEXT)";

        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + InventoryEntry.TABLE_NAME;
    }
}
