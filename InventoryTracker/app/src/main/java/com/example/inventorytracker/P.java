package com.example.inventorytracker;

import android.content.ContentValues;
import android.content.Context;
import android.widget.Toast;

public class P {
    public static Context c;
    private static boolean DEBUG_MODE = false;

    public static void p(String msg, String qualifier) {
        switch (qualifier) {
            case "user":
                print(msg);
                break;
            case "debug":
                if (DEBUG_MODE) {
                    print(msg);
                }
                break;
            default:
                break;
        }
    }

    private static void print(String msg) {
        Toast.makeText(c, msg, Toast.LENGTH_SHORT).show();
    }

    public static void passContextForOutputEngine(Context context){c = context;}
}
