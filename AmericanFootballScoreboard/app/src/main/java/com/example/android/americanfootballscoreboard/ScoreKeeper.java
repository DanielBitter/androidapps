package com.example.android.americanfootballscoreboard;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.InputType;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Dan on 10/11/2016.
 * Code snips used from these sources
 * http://stackoverflow.com/questions/10539268/making-two-linearlayouts-have-50-of-the-screen-each-without-using-layout-weight
 * http://stackoverflow.com/questions/12997273/alertdialog-with-edittext-open-soft-keyboard-automatically-with-focus-on-editte
 * http://stackoverflow.com/questions/11363209/alertdialog-with-positive-button-and-validating-custom-edittext
 * http://stackoverflow.com/questions/18799216/how-to-make-a-edittext-box-in-a-dialog
 * http://stackoverflow.com/questions/5706942/possibility-to-add-parameters-in-button-xml
 * http://stackoverflow.com/questions/10903754/input-text-dialog-android
 */
public class ScoreKeeper {
    Resources incomingRes;
    Activity incomingActivity;
    ArrayList<ScoreEvent> scoreProgressionTeams;

    String touchdown;
    String extraPoint;
    String fieldGoal;
    String twoPtConv;
    String safety;

    public ScoreKeeper(Activity activity, Resources res){
        this.incomingRes = res;
        this.incomingActivity = activity;
        scoreProgressionTeams = new ArrayList<ScoreEvent>();


        touchdown = incomingActivity.getString(R.string.scoreTypeOne);
        extraPoint = incomingActivity.getString(R.string.scoreTypeTwo);
        fieldGoal = incomingActivity.getString(R.string.scoreTypeThree);
        twoPtConv = incomingActivity.getString(R.string.scoreTypeFour);
        safety = incomingActivity.getString(R.string.scoreTypeFive);
    }

    public void updatedScoreboard(String info){
        boolean infoError = false;
        ScoreEvent scoreEvent = new ScoreEvent();
        String[] infoArray;
        String scoreType;
        String scoreTeam;
        TextView scoreboard = null;
        int score;

        try{
            infoArray = info.split(incomingRes.getString(R.string.arraySeparator));
            scoreType = infoArray[0];
            scoreTeam = infoArray[1];

            scoreEvent.setEventName(scoreType);
            scoreEvent.setScoringTeam(scoreTeam);

            scoreboard  = (TextView) incomingActivity.findViewById(R.id.scoreboard1a);

            if(scoreEvent.getScoringTeam().equalsIgnoreCase(
                    incomingRes.getString(R.string.teamBsubId))){
                scoreboard  = (TextView) incomingActivity.findViewById(R.id.scoreboard1b);
            }
            score = Integer.valueOf(scoreboard.getText().toString());
        }catch (Exception e){
            infoError = true;
            scoreEvent.setEventName(incomingActivity.getString(R.string.error));
            scoreEvent.setScoringTeam(incomingActivity.getString(R.string.error));
            score = 0;
        }
        int pointsScored = 0;

        if(!infoError && isLegalScore(scoreEvent)){
            String eventName = scoreEvent.getEventName();

            if(touchdown.equalsIgnoreCase(eventName)){
                pointsScored = incomingRes.getInteger(R.integer.scoreValOne);
            }else if(extraPoint.equalsIgnoreCase(eventName)){
                pointsScored = incomingRes.getInteger(R.integer.scoreValTwo);
            }else if(fieldGoal.equalsIgnoreCase(eventName)){
                pointsScored = incomingRes.getInteger(R.integer.scoreValThree);
            }else if(twoPtConv.equalsIgnoreCase(eventName)){
                pointsScored = incomingRes.getInteger(R.integer.scoreValFour);
            }else if(safety.equalsIgnoreCase(eventName)){
                pointsScored = incomingRes.getInteger(R.integer.scoreValFive);
            }

            score += pointsScored;
            scoreboard.setText(String.valueOf(score));

        }

    }

    private boolean isLegalScore(ScoreEvent scoreEvent){
        boolean isLegal = false;

        String currEventName = scoreEvent.getEventName();
        String currTeam = scoreEvent.getScoringTeam();

        ScoreEvent lastEvent = null;
        int numScores = scoreProgressionTeams.size();

        try{
            lastEvent = scoreProgressionTeams.get(numScores-1);
        }catch(Exception e){

        }


        if(touchdown.equalsIgnoreCase(currEventName) ||
                fieldGoal.equalsIgnoreCase(currEventName) ||
                safety.equalsIgnoreCase(currEventName)){

                    isLegal = true;
        }else if(extraPoint.equalsIgnoreCase(currEventName) ||
                twoPtConv.equalsIgnoreCase(currEventName)){

                    if(lastEvent.getEventName().equalsIgnoreCase(touchdown) &&
                            lastEvent.getScoringTeam().equalsIgnoreCase(scoreEvent.getScoringTeam())){
                        isLegal = true;
                    }
        }else{
            isLegal = false;
        }

        if (isLegal){
            scoreProgressionTeams.add(scoreEvent);
        }

        return isLegal;
    }

    public void reset(){
        scoreProgressionTeams.clear();
        TextView scoreboard  = (TextView) incomingActivity.findViewById(R.id.scoreboard1a);
        Button teamButton = (Button) incomingActivity.findViewById(R.id.teamNameBtnA);
        scoreboard.setText(incomingActivity.getString(R.string.str_zero));
        teamButton.setText(incomingActivity.getString(R.string.defaultTeamBtnText));

        scoreboard  = (TextView) incomingActivity.findViewById(R.id.scoreboard1b);
        teamButton = (Button) incomingActivity.findViewById(R.id.teamNameBtnB);
        scoreboard.setText(incomingActivity.getString(R.string.str_zero));
        teamButton.setText(incomingActivity.getString(R.string.defaultTeamBtnText));
    }

}
