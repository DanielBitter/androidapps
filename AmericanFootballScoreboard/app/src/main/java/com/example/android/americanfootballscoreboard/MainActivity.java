package com.example.android.americanfootballscoreboard;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ScoreKeeper scoreKeeper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scoreKeeper = new ScoreKeeper(this, getResources());
    }
    
    public void initiateScorePoints(View view){
        if(view != null){
            String arraySep = getArraySeparator();
            if(arraySep != null && arraySep.length() > 0){
                scorePoints(view, arraySep);
            }else{
                toast("array sep is null"); //test code
            }
        }else{
            toast("view is null"); //test code
        }
    }

    public void scorePoints(View view, String arraySeparator){
        // view = null; //test code
        if(view != null){
            //toast("view is good");  //test code
            String team = getTeamNameFrom(getResEntryNameFromId(view));
            if(view.getTag() != null){
                String rearScoreInfo = concatStrings(arraySeparator, team);
                String scoreInfo = getStringValueOf(view.getTag());
                scoreInfo = concatStrings(scoreInfo, rearScoreInfo);
                scoreKeeper.updatedScoreboard(scoreInfo);
            }
        }else{
            toast("view is null"); //test code
        }
    }
    
    public void updateTeamName(View view){
        String teamNamefull = getResEntryNameFromId(view);
        final String teamName = getTeamNameFrom(teamNamefull);
        final Button teamBtn = getButtonFromTeam(teamName);
        
        DialogObjects dObj = createDialogObjects();
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText userInput = new EditText(this); //used as final in method
        final InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        DialogInterface.OnClickListener posOnClickListen = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int idNotUsed) {
                String userInputTeamName = userInput.getText().toString();
                int maxNameLength = getResources().getInteger(R.integer.teamNameMaxLen);
                if(userInputTeamName.length() > maxNameLength){
                    userInputTeamName = userInputTeamName.substring(0, maxNameLength+1);
                }
                if(userInputTeamName.length() > 0){
                    teamBtn.setText(userInputTeamName);
                }
                
                hideKeyboard(im, userInput);
            }
        };
        DialogInterface.OnClickListener neutralOnClickListen = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int idNotUsed) {
                //empty because overridden below, only way to prevent dialog closing when button clicked
            }
        };
        DialogInterface.OnClickListener negOnClickListen = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int idNotUsed) {
                hideKeyboard(im, userInput);
                dialog.cancel();
            }
        };
        View.OnClickListener clearUserInput = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userInput.setText("");
                }
            };
        
        createInputDialog(teamName, dObj, teamBtn, builder, userInput, im, 
            posOnClickListen, neutralOnClickListen, negOnClickListen, clearUserInput);
    }
    
    public void toast(String msg){
        if(msg.length() > 0){
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
    }

    public void resetScoreboard(View view){
        scoreKeeper.reset();
    }
    
    public String getArraySeparator(){
        return getString(R.string.arraySeparator);
    }
    
    public DialogObjects createDialogObjects(){
        return new DialogObjects(
            getResources().getString(R.string.editTeamNameTitlePre),
            getResources().getString(R.string.editTeamNameTitlePost),
            getResources().getString(R.string.editTeamNameMsg),
            getResources().getString(R.string.defaultTeamBtnText),
            getString(R.string.teamBsubId),
            this.getString(R.string.editTeamNameLabelPositive),
            this.getString(R.string.editTeamNameLabelNeutral),
            this.getString(R.string.editTeamNameLabelNegative),
            DialogInterface.BUTTON_NEUTRAL,
            InputType.TYPE_CLASS_TEXT
        );
    }
    
    public String concatStrings(String s, String t){
        String retVal = "noRetVal";
        String error = "";
        try{
            retVal = s.concat(t);
        }catch(Exception e){
            error = e.toString();
        }
        error = (error.length() > 0) ? ("error: " + error) : "";
        // toast(retVal);
        // toast(error);
        return retVal;
    }

    public String getTeamNameFrom(String s){
        String retVal = "noRetVal";
        String error = "";
        try{
            retVal = s.substring(s.length()-1);
        }catch(Exception e){
            error = e.toString();
        }
        error = (error.length() > 0) ? ("error: " + error) : "";
        // toast(retVal);
        // toast(error);
        return retVal;
    }

    public String getResEntryNameFromId(View v){
        String retVal = "noRetVal";
        String error = "";
        try{
            retVal = getResources().getResourceEntryName(v.getId());
        }catch(Exception e){
            error = e.toString();
        }
        error = (error.length() > 0) ? ("error: " + error) : "";
        // toast(retVal);
        // toast(error);
        return retVal;
    }
    
    public String getStringValueOf(Object o){
        String retVal = "noRetVal";
        String error = "";
        try{
            retVal = o.toString();
        }catch(Exception e){
            error = e.toString();
        }
        error = (error.length() > 0) ? ("error: " + error) : "";
        // toast(retVal);
        // toast(error);
        return retVal;
    }
    
    public Button getButtonFromTeam(String name){
        Button teamButton = (Button) findViewById(R.id.teamNameBtnA);
        String teamBsubId = getString(R.string.teamBsubId);

        if(name.equalsIgnoreCase(teamBsubId)){
            teamButton = (Button) findViewById(R.id.teamNameBtnB);
        }
        
        return teamButton;
    }
    
    public void createInputDialog(  String teamName, 
                                    DialogObjects dialogObject, 
                                    Button teamButton, 
                                    AlertDialog.Builder builder, 
                                    final EditText inputField,
                                    final InputMethodManager im,
                                    DialogInterface.OnClickListener posOnClick,
                                    DialogInterface.OnClickListener neutralOnClick,
                                    DialogInterface.OnClickListener negOnClick,
                                    View.OnClickListener clearInput){
                                        
        builder.setTitle(   dialogObject.titlePre + " " +
                            teamName + " " +
                            dialogObject.titlePost);
        builder.setCancelable(true);
        
        String dialogMsg = dialogObject.message;
        builder.setMessage(dialogMsg);

        // Set up the input
        inputField.setInputType(dialogObject.inputType);
        String defaultTeamBtnTxt = dialogObject.defaultBtnText;
        String currTeamName = teamButton.getText().toString().trim();
        if(defaultTeamBtnTxt.startsWith(currTeamName)){
            currTeamName = "";
        }
        inputField.setText(currTeamName);
        builder.setView(inputField);

        // Set up the buttons
        final String teamNameFinalTempt = teamName;
        final String teamBsubIdFinal = dialogObject.bSubId;
        builder.setPositiveButton(dialogObject.positiveBtnLabel, posOnClick);
        builder.setNeutralButton(dialogObject.neutralBtnLabel, neutralOnClick);
        builder.setNegativeButton(dialogObject.negativeBtnLabel, negOnClick);

        AlertDialog alert = builder.create();
        alert.show();
        
        alert.getButton(dialogObject.neutralIntId).setOnClickListener(clearInput);

        inputField.requestFocus();
        showKeyboard(im);
    }
    
    public void showKeyboard(InputMethodManager im){ //?
        im.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
    
    public void hideKeyboard(InputMethodManager im, EditText inputField){ //?
        im.hideSoftInputFromWindow(inputField.getWindowToken(), 0); //bug - keyboard not hidden when dialog closed by touching outside
    }
}

class DialogObjects {
    public String titlePre;
    public String titlePost;
    public String message;
    public String defaultBtnText;
    public String bSubId;
    public String positiveBtnLabel;
    public String neutralBtnLabel;
    public String negativeBtnLabel;
    public int neutralIntId;
    public int inputType;
    
    public DialogObjects(String _pre, String _post, String _msg, String _btnTxt, String _bSubId, 
        String _positiveBtnLabel, String _neutralBtnLabel, String _negativeBtnLabel, int _neutralIntId, int _inputType){
        this.titlePre = _pre;
        this.titlePost = _post;
        this.message = _msg;
        this.defaultBtnText = _btnTxt;
        this.bSubId = _bSubId;
        this.positiveBtnLabel = _positiveBtnLabel;
        this.neutralBtnLabel = _neutralBtnLabel;
        this.negativeBtnLabel = _negativeBtnLabel;
        this.neutralIntId = _neutralIntId;
        this.inputType = _inputType;
    }
}
