package com.example.android.americanfootballscoreboard;

/**
 * Created by Dan on 10/11/2016.
 */
public class ScoreEvent {
    public ScoreEvent(){}

    private String eventName;
    private String scoringTeam;

    public void setEventName(String name){
        this.eventName = name;
    }
    public void setScoringTeam(String team){
        this.scoringTeam = team;
    }

    public String getEventName(){
        return this.eventName;
    }
    public String getScoringTeam(){
        return this.scoringTeam;
    }
}
