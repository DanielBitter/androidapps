Project Name: American Football Scoreboard

Author: Dan Bitter

Description: Udacity project #2. The app has one Activity split to two halves for opposing teams in a game of American football. The user can increase either team’s score, as allowed for by the game’s rules. Updates are reflected in a score board at the top, and there is no points limit. The user can reset the board to start over.

Deployment: The application can be loaded to Android Studio and built for an 
emulator or attached device. 

Requirements: Currently targeted for min sdk version 15

Contact/bug reports/feature requests:
bitter.daniel@gmail.com
linkedin.com/in/danielbitter

File list:
/app/manifests/AndroidManifest.xml
/app/java/com.example.android.americanfootballscoreboard/MainActivity.java
/app/java/com.example.android.americanfootballscoreboard/ScoreEvent.java
/app/java/com.example.android.americanfootballscoreboard/ScoreKeeper.java
/app/res/layout/activity_main.xml
/app/res/mipmap/ic_launcher.png
/app/res//values/colors.xml
/app/res//values/dimens.xml
/app/res//values/integers.xml
/app/res//values/strings.xml
/app/res//values/styles.xml