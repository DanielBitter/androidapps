package com.example.android.udacitynanoquiz;

import android.app.Activity;
import android.content.res.Resources;
import android.widget.Button;

import java.util.ArrayList;

/**
 * Created by Dan on 10/14/2016.
 */
public class ResultsData {
//    private Activity incomingActivity;
    public static ArrayList<QuestionData> answeredQuestions = new ArrayList<QuestionData>();
    public static int numCorrect = 0;

//    public ResultsData(Activity activity, Resources res){
////        this.incomingRes = res;
////        this.incomingActivity = activity;
//    }

    public static void addAnsweredQuestion(QuestionData qd){
        answeredQuestions.add(qd);
    }

    public static void addCorrect(){
        numCorrect+=1;
    }

    public static void resetCorrect(){
        numCorrect = 0;
    }

}
