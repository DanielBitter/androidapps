package com.example.android.udacitynanoquiz;

import android.graphics.Color;
import android.graphics.Interpolator;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/**
 * http://stackoverflow.com/questions/8089054/get-the-background-color-of-a-button-in-android
 * http://stackoverflow.com/questions/22639218/how-to-get-all-buttons-ids-in-one-time-on-android
 * http://stackoverflow.com/questions/10633456/how-to-easily-iterate-over-all-strings-within-the-strings-xml-resource-file
 *
 */
public class MainActivity extends AppCompatActivity {
    QuizMaster quizMaster = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        quizMaster = new QuizMaster(this);
//        quizMaster.resetQuestionButtons();
//        quizMaster.resetResultsData();
    }


    public void toggleText(View view){
        quizMaster.toggleTrueFalse();
    }

    public void toggleTrueFalse(View view){
//        quizMaster.toggleTrueFalse(view);
    }

    public void submitAnswer(View view){
        quizMaster.submitAnswer();
    }

    public void startQuiz(View view){
        quizMaster.startQuiz();
    }


}
/**
 <ScrollView
 xmlns:android="http://schemas.android.com/apk/res/android"
 xmlns:tools="http://schemas.android.com/tools"
 tools:context="com.example.android.udacitynanoquiz.MainActivity"
 style="@style/ScrollLayout" >

 <RelativeLayout
 android:id="@+id/RelativeLayoutParent"
 style="@style/RelativeLayout">
 */
