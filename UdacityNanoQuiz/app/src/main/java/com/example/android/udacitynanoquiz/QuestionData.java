package com.example.android.udacitynanoquiz;

/**
 * Created by Dan on 10/17/2016.
 */
public class QuestionData {
    private int id;
    private String questionTxt;
    private String answerTxt;
    private String questionType;
    private String correctAnswer;
    private String userAnswer;

    public QuestionData(){
    }

    public void setId(int identifier){this.id = identifier;}
    public void setQuestion(String questionTxt){this.questionTxt = questionTxt;}
    public void setAnswer(String answer){
        this.answerTxt = answer;
    }
    public void setQuestionType(String questionType){
        this.questionType = questionType;
    }
    public void setCorrectAnswer(String cAnswer){
        this.correctAnswer = cAnswer;
    }
    public void setUserAnswer(String uAnswer){this.userAnswer = uAnswer;}

    public int getId(){return this.id;}
    public String getQuestion(){
        return this.questionTxt;
    }
    public String getAnswer(){
        return this.answerTxt;
    }
    public String getQuestionType(){
        return this.questionType;
    }
    public String getCorrectAnswer(){
        return this.correctAnswer;
    }
    public String getUserAnswer(){
        return this.userAnswer;
    }
}
