package com.example.android.udacitynanoquiz;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Dan on 10/14/2016.
 * http://stackoverflow.com/questions/7618553/how-to-add-checkboxes-dynamically-in-android
 * http://stackoverflow.com/questions/6646442/creating-radiobuttons-programmatically
 * http://stackoverflow.com/questions/10766492/what-is-the-simplest-way-to-reverse-an-arraylist
 * http://stackoverflow.com/questions/3277196/can-i-set-androidlayout-below-at-runtime-programmatically
 * http://stackoverflow.com/questions/11194515/android-get-value-of-the-selected-radio-button
 * http://stackoverflow.com/questions/9883089/how-to-remove-a-layout-that-was-added-using-addcontentview
 * http://www.mysamplecode.com/2013/04/android-switch-button-example.html
 *
 */
public class QuizMaster {
    private Activity incomingActivity;
    private ArrayList<QuestionData> questionDataList = null;
    QuestionData currQuestion = null;
    private RelativeLayout rLayout = null;

    public QuizMaster(Activity activity){
        this.incomingActivity = activity;
        this.rLayout = (RelativeLayout) activity.findViewById(R.id.RelativeLayoutParent);
    }

    public void startQuiz(){
        questionDataList = loadQuestions();
        View startQuizBtn = incomingActivity.findViewById(R.id.startQuiz);
        startQuizBtn.setVisibility(View.GONE);
        displayQuestion();
    }

    public void displayQuestion(){
        if(0 < questionDataList.size() && rLayout != null){
            TextView questionTxt = (TextView) rLayout.findViewById(R.id.questionText);
            RadioGroup rgrp = (RadioGroup) rLayout.findViewById(R.id.radioGroup);
            EditText userInput = (EditText) rLayout.findViewById(R.id.editText);
            TextView trueFalseText = (TextView) rLayout.findViewById(R.id.trueFalseText);
            ToggleButton trueFalseSwitch = (ToggleButton) rLayout.findViewById(R.id.trueFalseToggle);

            try{
                rgrp.setVisibility(View.INVISIBLE);
                showCheckboxes(false);
                userInput.setVisibility(View.INVISIBLE);
                trueFalseSwitch.setVisibility(View.INVISIBLE);
            }catch (Exception e){
                //TODO null object after submit
            }
            currQuestion = questionDataList.get(0);
            questionTxt.setVisibility(View.VISIBLE);
            questionTxt.setText(" ");
            questionTxt.setText(currQuestion.getQuestion());
            String[] answerOptions = currQuestion.getAnswer().split(":");

            switch (currQuestion.getQuestionType()) {
                case "radio":
                    try{
                        rgrp.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        //TODO null object after submit
                    }
                    if(answerOptions.length == rgrp.getChildCount()){
                        for(int i=0;i<rgrp.getChildCount();++i){
                            RadioButton rbtn = (RadioButton) rgrp.getChildAt(i);
                            rbtn.setVisibility(View.VISIBLE);
                            rbtn.setText(" ");
                            rbtn.setText(answerOptions[i]);
                        }
                    }
                    break;

                case "check":
                    CheckBox cb0 = (CheckBox) incomingActivity.findViewById(R.id.checkbox0);
                    CheckBox cb1 = (CheckBox) incomingActivity.findViewById(R.id.checkboxOne);
                    CheckBox cb2 = (CheckBox) incomingActivity.findViewById(R.id.checkboxTwo);
                    CheckBox cb3 = (CheckBox) incomingActivity.findViewById(R.id.checkboxThree);

                    cb0.setVisibility(View.VISIBLE);
                    cb0.setText(answerOptions[0]);
                    cb1.setVisibility(View.VISIBLE);
                    cb1.setText(answerOptions[1]);
                    cb2.setVisibility(View.VISIBLE);
                    cb2.setText(answerOptions[2]);
                    cb3.setVisibility(View.VISIBLE);
                    cb3.setText(answerOptions[3]);

                    break;

                case "input":
                    try{
                        userInput.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        //TODO null object after submit
                    }
                    break;

                case "truefalse":
                    try{
                        trueFalseSwitch.setVisibility(View.VISIBLE);
                        trueFalseText.setVisibility(View.VISIBLE);
                        toggleTrueFalse();
//                        trueFalseText.setText(incomingActivity.getString(R.string.truefalseOff));
                        trueFalseSwitch.setTextOff(incomingActivity.getString(R.string.truefalseOff));
                        trueFalseSwitch.setTextOn(incomingActivity.getString(R.string.truefalseOn));
//                        trueFalseSwitch.setShowText(true);
//                        trueFalseSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//                            @Override
//                            public void onCheckedChanged(CompoundButton buttonView,
//                                                         boolean isChecked) {
//                                if(isChecked){
////                                    trueFalseText.setText(incomingActivity.getString(R.string.trueFalseText).toString().concat(
////                                            incomingActivity.getString(R.string.truefalseOn).toString()
////                                    ));
//                                }else{
////                                    trueFalseText.setText(incomingActivity.getString(R.string.trueFalseText).toString().concat(
////                                            incomingActivity.getString(R.string.truefalseOff).toString()
////                                    ));
//                                }
//                            }
//                        });
                    }catch (Exception e){
                        //TODO null object after submit
                    }
                    break;
            }
            //show submit button
            Button submitAnswer = (Button) incomingActivity.findViewById(R.id.submitAnswer);
            submitAnswer.setVisibility(View.VISIBLE);

            Button resetQuiz = (Button) incomingActivity.findViewById(R.id.resetQuiz);
            resetQuiz.setVisibility(View.VISIBLE);
        }else{
            //no more questions
            String results = ResultsData.numCorrect + " / " + "8 correct";
            Toast.makeText(incomingActivity.getApplicationContext(),
                    results, Toast.LENGTH_LONG).show();
        }
    }

    public void toggleTrueFalse(){
        TextView trueFalseTxt = (TextView) incomingActivity.findViewById(R.id.trueFalseText);
        ToggleButton trueFalseToggle = (ToggleButton) incomingActivity.findViewById(R.id.trueFalseToggle);
        String trueFalseDisplayString = trueFalseTxt.getText().toString();
        if(-1 < trueFalseDisplayString.indexOf(incomingActivity.getString(R.string.truefalseOff))){
            trueFalseTxt.setText(
                    incomingActivity.getString(R.string.trueFalseText).concat(
                        incomingActivity.getString(R.string.truefalseOn)
            ));
            trueFalseToggle.setBackgroundColor(incomingActivity.getColor(R.color.green));
        }else{
            trueFalseTxt.setText(
                    incomingActivity.getString(R.string.trueFalseText).concat(
                            incomingActivity.getString(R.string.truefalseOff)
                    ));
            trueFalseToggle.setBackgroundColor(incomingActivity.getColor(R.color.red));
        }
    }

    public String getCheckboxSelections(){
        String selections = "";
        String separator = incomingActivity.getString(R.string.separator);
        CheckBox cb0 = (CheckBox) incomingActivity.findViewById(R.id.checkbox0);
        CheckBox cb1 = (CheckBox) incomingActivity.findViewById(R.id.checkboxOne);
        CheckBox cb2 = (CheckBox) incomingActivity.findViewById(R.id.checkboxTwo);
        CheckBox cb3 = (CheckBox) incomingActivity.findViewById(R.id.checkboxThree);
        try{
            if(cb0.isChecked()){
                selections = selections.concat(separator.concat(cb0.getText().toString()));
            }
            if(cb1.isChecked()){
                selections = selections.concat(separator.concat(cb1.getText().toString()));
            }
            if(cb2.isChecked()){
                selections = selections.concat(separator.concat(cb2.getText().toString()));
            }
            if(cb3.isChecked()){
                selections = selections.concat(separator.concat(cb3.getText().toString()));
            }
        }catch (Exception e){
            selections = ""; //TODO
        }
        if(selections.startsWith(separator)){
            selections = selections.substring(1);
        }
        return selections;
    }

    public void showCheckboxes(boolean shouldShow){
        CheckBox cb0 = (CheckBox) incomingActivity.findViewById(R.id.checkbox0);
        CheckBox cb1 = (CheckBox) incomingActivity.findViewById(R.id.checkboxOne);
        CheckBox cb2 = (CheckBox) incomingActivity.findViewById(R.id.checkboxTwo);
        CheckBox cb3 = (CheckBox) incomingActivity.findViewById(R.id.checkboxThree);

        if(shouldShow){
            cb0.setVisibility(View.VISIBLE);
            cb1.setVisibility(View.VISIBLE);
            cb2.setVisibility(View.VISIBLE);
            cb3.setVisibility(View.VISIBLE);
        }else{
            cb0.setVisibility(View.INVISIBLE);
            cb1.setVisibility(View.INVISIBLE);
            cb2.setVisibility(View.INVISIBLE);
            cb3.setVisibility(View.INVISIBLE);
        }
    }

    public void submitAnswer(){
        if(0 < questionDataList.size()){
            View view = null;
            ResultsData.addAnsweredQuestion(questionDataList.get(0));
            String correctAnswer = questionDataList.get(0).getCorrectAnswer();
            String userAnswer = incomingActivity.getString(R.string.noUserAnswer);
            String type = questionDataList.get(0).getQuestionType();

            switch (type){
                case "radio":
                    RadioGroup radioGroup = (RadioGroup) incomingActivity.findViewById(R.id.radioGroup); //create the RadioGroup

                    if(radioGroup.getCheckedRadioButtonId()!=-1) {
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) incomingActivity.findViewById(selectedId);
                        userAnswer = (String) radioButton.getText().toString();

                        if (correctAnswer.equalsIgnoreCase(userAnswer)) {
                            ResultsData.addCorrect();
                        }
                        questionDataList.remove(0);
                        displayQuestion();
                    }
                    break;

                case "check":
                    userAnswer = getCheckboxSelections();

                    if(correctAnswer.equalsIgnoreCase(userAnswer)){
                        ResultsData.addCorrect();
                    }
                    questionDataList.remove(0);
                    displayQuestion();
                    break;

                case "input":
                    EditText userInput = (EditText) incomingActivity.findViewById(R.id.editText);
                    userAnswer = userInput.getText().toString();
                    if(correctAnswer.equalsIgnoreCase(userAnswer)){
                        ResultsData.addCorrect();
                    }
                    questionDataList.remove(0);
                    displayQuestion();
                    break;

                case "truefalse":
                    ToggleButton trueFalseToggle = (ToggleButton) incomingActivity.findViewById(R.id.trueFalseToggle);
//                    userAnswer = trueFalseToggle.getText().toString();
                    userAnswer = String.valueOf(trueFalseToggle.isChecked());
                    if(correctAnswer.equalsIgnoreCase(userAnswer)){
                        ResultsData.addCorrect();
                    }
                    questionDataList.remove(0);
                    displayQuestion();
                    break;
            }

        }
    }

    public void resetQuiz(){
        ViewGroup parentLayout = (ViewGroup) incomingActivity.findViewById(R.id.RelativeLayoutParent);
//        parentLayout.
    }

    public ArrayList<QuestionData> loadQuestions(){
        ArrayList<QuestionData> questionDataList = new ArrayList<QuestionData>();
        String[] questionDataType = incomingActivity.getResources().getStringArray(R.array.questionDataTypes);
        String[] questionDataQuestions = incomingActivity.getResources().getStringArray(R.array.questionDataQuestions);
        String[] questionDataAnswers = incomingActivity.getResources().getStringArray(R.array.questionDataAnswers);
        String[] questionDataCorrectAnswers = incomingActivity.getResources().getStringArray(R.array.questionDataCorrectAnswers);

        if (
                questionDataType.length == questionDataQuestions.length &&
                        questionDataQuestions.length == questionDataAnswers.length &&
                        questionDataAnswers.length == questionDataCorrectAnswers.length &&
                        questionDataCorrectAnswers.length == questionDataType.length
                ) {

                    for (int i = 0; i < questionDataType.length; ++i) {
                        QuestionData qd = new QuestionData();
                        String[] separateParts = questionDataAnswers[i].split(
                                incomingActivity.getString(R.string.separator));

                        qd.setId(Util.generateViewId());
                        qd.setQuestionType(questionDataType[i]);
                        qd.setQuestion(questionDataQuestions[i]);
                        qd.setAnswer(questionDataAnswers[i]);
                        qd.setCorrectAnswer(questionDataCorrectAnswers[i]);

                        questionDataList.add(qd);
                    }

        } else {
            //TODO not a 1:1:1 relationship between data strings
        }
        return questionDataList;
    }

}
