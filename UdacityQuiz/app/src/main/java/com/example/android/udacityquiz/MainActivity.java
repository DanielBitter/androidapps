package com.example.android.udacityquiz;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * http://abhiandroid.com/ui/togglebutton
 * https://www.mkyong.com/android/android-radio-buttons-example/
 * https://www.mkyong.com/android/android-checkbox-example/
 * http://stackoverflow.com/questions/6674341/how-to-use-scrollview-in-android
 * http://stackoverflow.com/questions/8271406/how-to-make-a-static-button-under-a-scrollview
 * http://stackoverflow.com/questions/2892615/how-to-remove-auto-focus-keyboard-popup-of-a-field-when-the-screen-shows-up
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void submitQuiz(View view){
        boolean perfectScore = true;
        int numericalScore = 0;

        TextView questionOne = (TextView) findViewById(R.id.questionOne);
        TextView questionTwo = (TextView) findViewById(R.id.questionTwo);
        TextView questionThree = (TextView) findViewById(R.id.questionThree);
        TextView questionFour = (TextView) findViewById(R.id.questionFour);
        TextView questionFive = (TextView) findViewById(R.id.questionFive);
        TextView questionSix = (TextView) findViewById(R.id.questionSix);
        TextView questionSeven = (TextView) findViewById(R.id.questionSeven);

        RadioGroup rgOne = (RadioGroup) findViewById(R.id.radioGrpOne);
        EditText editTextTwo = (EditText) findViewById(R.id.editTextTwo);
        EditText editTextThree = (EditText) findViewById(R.id.editTextThree);
        ToggleButton toggleFour = (ToggleButton) findViewById(R.id.toggleTwo);
        CheckBox checkBoxFiveOne = (CheckBox) findViewById(R.id.chkOneGrpOne);
        CheckBox checkBoxFiveTwo = (CheckBox) findViewById(R.id.chkOneGrpTwo);
        CheckBox checkBoxFiveThree = (CheckBox) findViewById(R.id.chkOneGrpThree);
        CheckBox checkBoxFiveFour = (CheckBox) findViewById(R.id.chkOneGrpFour);
        ToggleButton toggleSix= (ToggleButton) findViewById(R.id.toggleSix);
        ToggleButton toggleSeven = (ToggleButton) findViewById(R.id.toggleSeven);

        int answerOneId = rgOne.getCheckedRadioButtonId();
        RadioButton answerOneRadio = (RadioButton) findViewById(answerOneId);
        if(getString(R.string.itemOneAnswer).toString().equalsIgnoreCase(
                answerOneRadio.getText().toString().trim()
        )){
            questionOne.setTextColor(Color.GREEN);
            ++numericalScore;
        }
        else
        {
            perfectScore = false;
            questionOne.setTextColor(Color.RED);
        }

        if(getString(R.string.itemTwoAnswer).toString().equalsIgnoreCase(
                editTextTwo.getText().toString().trim()
        )){
            questionTwo.setTextColor(Color.GREEN);
            ++numericalScore;
        }
        else
        {
            perfectScore = false;
            questionTwo.setTextColor(Color.RED);
        }

        if(getString(R.string.itemThreeAnswer).toString().equalsIgnoreCase(
                editTextThree.getText().toString().trim()
        )){
            questionThree.setTextColor(Color.GREEN);
            ++numericalScore;
        }
        else
        {
            perfectScore = false;
            questionThree.setTextColor(Color.RED);
        }

        if(getString(R.string.itemFourAnswer).toString().equalsIgnoreCase(
                String.valueOf(toggleFour.isChecked())
        )){
            questionFour.setTextColor(Color.GREEN);
            ++numericalScore;
        }
        else
        {
            perfectScore = false;
            questionFour.setTextColor(Color.RED);
        }

        String questionFiveCheckedAnswers = "";
        String separator = getString(R.string.separator);
        if(checkBoxFiveOne.isChecked()){
            questionFiveCheckedAnswers = questionFiveCheckedAnswers.concat(
                    separator.concat(checkBoxFiveOne.getText().toString()));
        }
        if(checkBoxFiveTwo.isChecked()){
            questionFiveCheckedAnswers = questionFiveCheckedAnswers.concat(
                    separator.concat(checkBoxFiveTwo.getText().toString()));
        }
        if(checkBoxFiveThree.isChecked()){
            questionFiveCheckedAnswers = questionFiveCheckedAnswers.concat(
                    separator.concat(checkBoxFiveThree.getText().toString()));
        }
        if(checkBoxFiveFour.isChecked()){
            questionFiveCheckedAnswers = questionFiveCheckedAnswers.concat(
                    separator.concat(checkBoxFiveFour.getText().toString()));
        }
        if(questionFiveCheckedAnswers.length() > 0){
            questionFiveCheckedAnswers = questionFiveCheckedAnswers.substring(1);
        }
        if(getString(R.string.itemFiveAnswer).toString().equalsIgnoreCase(
                questionFiveCheckedAnswers)
        ){
            questionFive.setTextColor(Color.GREEN);
            ++numericalScore;
        }
        else
        {
            perfectScore = false;
            questionFive.setTextColor(Color.RED);
        }

        if(getString(R.string.itemSixAnswer).toString().equalsIgnoreCase(
                String.valueOf(toggleSix.isChecked())
        )){
            questionSix.setTextColor(Color.GREEN);
            ++numericalScore;
        }
        else
        {
            perfectScore = false;
            questionSix.setTextColor(Color.RED);
        }

        if(getString(R.string.itemSevenAnswer).toString().equalsIgnoreCase(
                String.valueOf(toggleSeven.isChecked())
        )){
            questionSeven.setTextColor(Color.GREEN);
            ++numericalScore;
        }
        else
        {
            perfectScore = false;
            questionSeven.setTextColor(Color.RED);
        }

        String perfectScoreStr = getString(R.string.perfectScore);
        String notPerfectScoreStr = getString(R.string.notPerfectScore);
        String result = numericalScore + "/7";
        if(perfectScore){
            result = String.format(perfectScoreStr, result);
            Toast.makeText(MainActivity.this,
                    result, Toast.LENGTH_LONG).show();
        }
        else
        {
            result = String.format(notPerfectScoreStr, result);
            Toast.makeText(MainActivity.this,
                    result, Toast.LENGTH_LONG).show();
        }
    }
}
