Project Name: Basketball Score Counter

Author: Dan Bitter

Description: Udacity course app for teaching User Interfaces and Input. The app shows one initial image of a cookie, a sentence, and a button labelled “Eat Cookie.” The picture is changed to a cookie with a bite taken and difference sentence when the button is clicked. There’s no current way to reset the app other than killing and relaunching.

Deployment: The application can be loaded to Android Studio and built for an 
emulator or attached device. 

Requirements: Currently targeted for min sdk version 15

Contact/bug reports/feature requests:
bitter.daniel@gmail.com
linkedin.com/in/danielbitter

File list:
/app/manifests/AndroidManifest.xml
/app/java/com.example.android.cookies/MainActivity.java
/app/res/drawable/after_cookie.jpg
/app/res/drawable/before_cookie.jpg
/app/res/layout/activity_main.xml
/app/res/mipmap/ic_launcher.png
/app/res/values/colors.xml
/app/res/values/dimens.xml
/app/res/values/strings.xml
/app/res/values/styles.xml