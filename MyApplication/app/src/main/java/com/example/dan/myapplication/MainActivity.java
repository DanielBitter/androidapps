package com.example.dan.myapplication;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        phone = getString(R.string.businessPhone);

        TextView businessName = (TextView) findViewById(R.id.businessName);
        TextView businessPhone = (TextView) findViewById(R.id.businessPhone);
        TextView businessAddr = (TextView) findViewById(R.id.businessAddress);
        businessName.setText(getString(R.string.businessName)
                .concat("\n" + getString(R.string.businessDesc))
        );
        businessPhone.setText(getString(R.string.businessPhone));
        businessAddr.setText(getString(R.string.businessAddress));
    }

    public void callBusiness(View view){
        String uri = "tel:" + phone.trim() ;
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    public void navToBusiness(View view){
        String geoAddrStr = "geo:" + "43.024883" + "," +
                "-87.912829" + "?q=" + "613" + "+" +
                "S+2nd+st" + "+" + "Milwaukee,WI" + "+" + "53204";
        Toast.makeText(this, geoAddrStr, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(geoAddrStr));

        PackageManager manager = getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            startActivity(intent);
        }else{
            String url = "https://goo.gl/vDO9Mk";
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    }
}
