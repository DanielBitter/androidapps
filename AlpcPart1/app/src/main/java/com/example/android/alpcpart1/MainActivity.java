package com.example.android.alpcpart1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void encodeInt(View view){
        EditText userInput = (EditText) findViewById(R.id.encodeUserInput);
        TextView displayResult = (TextView) findViewById(R.id.displayEncodeResults);

        int signedInt = getResources().getInteger(R.integer.signedInt);
        int limit_upper = getResources().getInteger(R.integer.limit_upper);
        int limit_lower = getResources().getInteger(R.integer.limit_lower);
        try{
            signedInt = Integer.valueOf(userInput.getText().toString());
        } catch(Exception e){
            displayResult.setText(e.toString());
        }
        if(signedInt<limit_lower || signedInt>limit_upper || signedInt/1!=signedInt){
            displayResult.setText(getString(R.string.error_invalidInput));
        }else{
            displayResult.setText(encodeSignedInt(signedInt));
        }
    }

    public String encodeSignedInt(int signedInt){
        String encodeMsg = "";
        int unSignedTranslatedNum = translateRange(signedInt);
        encodeMsg=encodeMsg
                .concat(getString(R.string.result_unsignedNumber))
                .concat(String.valueOf(unSignedTranslatedNum));

        String intermediateBinaryString = convertDecimalToBinary(unSignedTranslatedNum);
        int decimal = Integer.parseInt(intermediateBinaryString,
                getResources().getInteger(R.integer.baseValue_2));
        String hexStr = Integer.toString(decimal,
                getResources().getInteger(R.integer.baseValue_16));
        hexStr = insertPadding(hexStr,
                getResources().getInteger(R.integer.baseValue_4));
        encodeMsg=encodeMsg
                .concat(getString(R.string.result_intermediate))
                .concat(hexStr);

        String encodedBinaryString = encode(intermediateBinaryString);
        decimal = Integer.parseInt(encodedBinaryString,
                getResources().getInteger(R.integer.baseValue_2));
        hexStr = Integer.toString(decimal,
                getResources().getInteger(R.integer.baseValue_16));
        hexStr = insertPadding(hexStr,
                getResources().getInteger(R.integer.baseValue_4));
        encodeMsg=encodeMsg
                .concat(getString(R.string.result_encoded))
                .concat(hexStr);

        return encodeMsg;
    }

    public String insertPadding(String unpadded, int targetLength){
        int numOfNeededZeroes = targetLength-unpadded.length();
        String padded = unpadded;
        String zero = getString(R.string.str_zero);
        for(int index=0;index<numOfNeededZeroes;index++){
            padded = zero.concat(padded);
        }
        return padded;
    }

    public String encode(String binaryNumberString){
        int numClearedBits = getResources().getInteger(R.integer.numClearedBits);
        int numHighBits = getResources().getInteger(R.integer. numHighBits);
        int numLowBits = getResources().getInteger(R.integer.numLowBits);
        String encodedBinaryNumString = "";

        String highBits = binaryNumberString.substring(numClearedBits,numClearedBits+numHighBits);
        int highBitsInt = Integer.parseInt(highBits,
                getResources().getInteger(R.integer.baseValue_2));

        String lowBits = binaryNumberString.substring(numClearedBits+numHighBits);
        int lowBitsInt = Integer.parseInt(lowBits,
                getResources().getInteger(R.integer.baseValue_2));

        int shiftBits = getResources().getInteger(R.integer.shiftHighBits);
        highBitsInt = highBitsInt<<shiftBits;
        int encodedBinaryNum = highBitsInt | lowBitsInt;

        encodedBinaryNumString = Integer.toBinaryString(encodedBinaryNum);
        return encodedBinaryNumString;
    }

    public String convertDecimalToBinary(int decimalInt){
        String decimalString = "";
        decimalString = Integer.toBinaryString(decimalInt);
        decimalString = insertPadding(decimalString,
                getResources().getInteger(R.integer.baseValue_16));
        return decimalString;
    }

    public int translateRange(int signedInt){
        int offset = getResources().getInteger(R.integer.limit_offset);
        int unsignedInt = signedInt + offset;
        return unsignedInt;
    }


    public void validateAndDecode(View view){
        EditText userInput = (EditText) findViewById(R.id.editText2);
        TextView displayResult = (TextView) findViewById(R.id.textView4);

        String hexString = userInput.getText().toString();
        String testInput = "";
        try{
            testInput = String.valueOf(Integer.parseInt(hexString,
                    getResources().getInteger(R.integer.baseValue_16)));
        }catch(Exception e){
            hexString = "invalid input";
        }
        if(hexString.length() > 4){
            displayResult.setText(getString(R.string.error_invalidInput));
        }else{
            hexString = insertPadding(hexString,
                    getResources().getInteger(R.integer.baseValue_4));
            displayResult.setText(decodeHex(hexString));
        }
    }

    public String decodeHex(String hexString){
        String result = "";
        String byteOneHex = decode(hexString.substring(0,2));
        result=result
                .concat(getString(R.string.hexVal1)
                .concat(hexString.substring(0,2)));
        String byteTwoHex = decode(hexString.substring(2));
        result=result
                .concat(getString(R.string.hexVal2)
                .concat(hexString.substring(2)));

        String intermediate = getIntermedFromEncoded(byteOneHex, byteTwoHex);
        int signedInt = getSignedInt(intermediate);
        result=result
                .concat(getString(R.string.decoded)
                .concat(String.valueOf(signedInt)));

        return result;
    }

    public int getSignedInt(String intermediate){
        int unsignedInt = Integer.parseInt(intermediate,
                getResources().getInteger(R.integer.baseValue_2));
        int signedInt = unsignedInt - getResources().getInteger(R.integer.limit_offset);
        return signedInt;
    }

    public String getIntermedFromEncoded(String byteOne, String byteTwo){
        //substring(1) to remove leading 0
        String intermediateNum = byteOne.substring(1).concat(byteTwo.substring(1));
        return intermediateNum;
    }

    public String decode(String hexStr){
        int hexToInt = Integer.parseInt(hexStr,
                getResources().getInteger(R.integer.baseValue_16));
        String binaryNum = Integer.toBinaryString(hexToInt);
        binaryNum = insertPadding(binaryNum,
                getResources().getInteger(R.integer.baseValue_8));
        return binaryNum;
    }
}
