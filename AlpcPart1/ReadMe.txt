Project Name: AlpcPart1

Author: Dan Bitter

Description: Job application challenge #1 from Art & Logic programming. Application
 receives user input in the top text field as a number from -8192 to +8191 and encodes to a 4 digit hex string. The app also receives user input in the bottom text field as a 4 digit hex string and decodes to a number from -8192 to +8191.

Deployment: The application can be loaded to Android Studio and built for an 
emulator or attached device. 

Requirements: Currently targeted for min sdk version 15

Contact/bug reports/feature requests:
bitter.daniel@gmail.com
linkedin.com/in/danielbitter

File list:
/app/manifests/AndroidManifest.xml
/app/java/com.example.android.alpcpart1/MainActivity.java
/app/res/layout/activity_main.xml
/app/res/mipmap/ic_launcher.png
/app/res/values/colors.xml
/app/res/values/dimens.xml
/app/res/values/integers.xml
/app/res/values/strings.xml
/app/res/values/styles.xml