package com.example.android.miwok;

/**
 * Created by danielbitter on 12/7/16.
 */

public class Word {
    private String mMiwokTranslation;
    private String mDefaultTranslation;
    /**
     * NO_IMAGE_PROVIDED is correct way to check if images are present
     * try/catch  in WordAdapter works for this specific case
     * eventually see it doesnt work when background colors are applied
     */
    private int imageResourceId = NO_IMAGE_PROVIDED;
    private static final int NO_IMAGE_PROVIDED = -1;
    private int audioResourceId = NO_AUDIO_PROVIDED;
    private static final int NO_AUDIO_PROVIDED = -1;

    public Word(String defaultTranslation, String miwokTranslation, int audioId){
        this.mMiwokTranslation = miwokTranslation;
        this.mDefaultTranslation = defaultTranslation;
        this.audioResourceId = audioId;

    }

    public Word(String defaultTranslation, String miwokTranslation, int audioId, int resId){
        this.mMiwokTranslation = miwokTranslation;
        this.mDefaultTranslation = defaultTranslation;
        this.audioResourceId = audioId;
        this.imageResourceId = resId;
    }

    @Override
    public String toString() {
        return "Word{" +
                "mMiwokTranslation='" + mMiwokTranslation + '\'' +
                ", mDefaultTranslation='" + mDefaultTranslation + '\'' +
                ", imageResourceId=" + imageResourceId +
                ", audioResourceId=" + audioResourceId +
                '}';
    }

    public String getMiwokTranslation(){
        return this.mMiwokTranslation;
    }

    public String getEnglishTranslation(){
        return this.mDefaultTranslation;
    }

    public int getImageResourceId(){
        return this.imageResourceId;
    }

    public boolean hasImage(){
        return imageResourceId != NO_IMAGE_PROVIDED;
    }

    public int getAudioResourceId(){
        return this.audioResourceId;
    }

    public boolean hasAudio(){
        return audioResourceId != NO_AUDIO_PROVIDED;
    }
}
