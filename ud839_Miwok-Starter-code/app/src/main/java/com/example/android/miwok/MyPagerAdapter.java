package com.example.android.miwok;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by danielbitter on 12/11/16.
 * https://guides.codepath.com/android/google-play-style-tabs-using-tablayout#sliding-tabs-layout
 */

public class MyPagerAdapter extends FragmentPagerAdapter {
    private String tabTitles[];

    public MyPagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        tabTitles = new String[4];
        tabTitles[0] = context.getString(R.string.category_numbers);
        tabTitles[1] = context.getString(R.string.category_family);
        tabTitles[2] = context.getString(R.string.category_colors);
        tabTitles[3] = context.getString(R.string.category_phrases);
    }

    @Override
    public Fragment getItem(int pos) {
        switch(pos) {
            case 0: return new NumbersFragment();
            case 1: return new FamilyFragment();
            case 2: return new ColorsFragment();
            case 3: return new PhrasesFragment();
            default: return new NumbersFragment();
        }
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
