package com.example.android.miwok;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ColorsFragment extends Fragment {
    MediaPlayer mediaPlayer = null;
    AudioManager audioManager = null;
    AudioManager.OnAudioFocusChangeListener afChangeListener =
            new AudioManager.OnAudioFocusChangeListener(){
                @Override
                public void onAudioFocusChange(int focusChange) {
                    switch (focusChange){
                        case AudioManager.AUDIOFOCUS_GAIN:
                            mediaPlayer.start();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            releaseMediaPlayer();
                            break;
                        /** loss & loss transient both result in pause
                         * seekTo 0: whole pronunciation needs to be heard
                         */
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            mediaPlayer.pause();
                            mediaPlayer.seekTo(0);
                            break;
                        default:
                            Log.e(this.toString(),"Unaccounted audio state");
                            break;
                    }
                }
            };

    MediaPlayer.OnCompletionListener onCompleteListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };

    public ColorsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.word_list, container, false);
        String[] wordsArray = new String[]{
                "red",
                "green",
                "brown",
                "gray",
                "black",
                "white",
                "dusty yellow",
                "mustard yellow"};
        String[] miwokArray = new String[]{
                "weṭeṭṭi",
                "chokokki",
                "ṭakaakki",
                "ṭopoppi",
                "kululli",
                "kelelli",
                "ṭopiisә",
                "chiwiiṭә"
        };
        int[] audioArray = new int[]{
                R.raw.color_red,
                R.raw.color_green,
                R.raw.color_brown,
                R.raw.color_gray,
                R.raw.color_black,
                R.raw.color_white,
                R.raw.color_dusty_yellow,
                R.raw.color_mustard_yellow
        };

        int[] imageArray = new int[]{
                R.drawable.color_red,
                R.drawable.color_green,
                R.drawable.color_brown,
                R.drawable.color_gray,
                R.drawable.color_black,
                R.drawable.color_white,
                R.drawable.color_dusty_yellow,
                R.drawable.color_mustard_yellow
        };

        final ArrayList<Word> words = new ArrayList<Word>();
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

        for(int i=0;i<wordsArray.length;++i){
            words.add(i, new Word(wordsArray[i], miwokArray[i], audioArray[i], imageArray[i]));
            Log.v("ColorsActivity"+this.getClass().toString(),
                    "Word at index "+i+": "+words.get(i));
        }

        WordAdapter adapter = new WordAdapter(getActivity(), words, R.color.category_colors);
        ListView listView = (ListView) rootView.findViewById(R.id.list);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v("NumbersActivity", "Current word: " + words.get(position).toString());
                int audioId = words.get(position).getAudioResourceId();
                releaseMediaPlayer();

                int result = audioManager.requestAudioFocus(
                        afChangeListener,
                        AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    mediaPlayer = MediaPlayer.create(getActivity(), audioId);
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(onCompleteListener);
                }
            }
        });

        return rootView;
    }


    private void releaseMediaPlayer(){
        if(mediaPlayer != null){
            mediaPlayer.release();
            mediaPlayer = null;
            audioManager.abandonAudioFocus(afChangeListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }
}
