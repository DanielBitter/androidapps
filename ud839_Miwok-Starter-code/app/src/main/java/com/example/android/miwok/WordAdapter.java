package com.example.android.miwok;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by danielbitter on 12/7/16.
 */

public class WordAdapter extends ArrayAdapter<Word> {
    private final Context context;
    private final ArrayList<Word> values;
    private int colorId;

    public WordAdapter(Context context, ArrayList<Word> values, int color) {
        super(context, 0, values);
        this.context = context;
        this.values = values;
        this.colorId = color;
    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        TextView miwokTextView = (TextView) listItem.findViewById(R.id.miwok_text_view);
        TextView defaultTextView = (TextView) listItem.findViewById(R.id.default_text_view);
        ImageView image = (ImageView) listItem.findViewById(R.id.image);
        LinearLayout layout = (LinearLayout) listItem.findViewById(R.id.activityParentLayout);
        ImageView playIcon = (ImageView) listItem.findViewById(R.id.playIcon);
        try{
            int color = ContextCompat.getColor(getContext(), this.colorId);
            layout.setBackgroundColor(color);
            Word word = values.get(position);
            miwokTextView.setText(word.getMiwokTranslation());
            defaultTextView.setText(word.getEnglishTranslation());
            /**
             * try/catch results in correct behavior of showing/not showing
             * order of attr sets probably matters
             * probably breaks if more code is expected after image set
             * eventually see it breaks when background colors are applied
             */
            if(word.hasImage()){
                image.setImageResource(word.getImageResourceId());
                image.setVisibility(View.VISIBLE);
            }else{
                image.setVisibility(View.GONE);
            }
            playIcon.setImageResource(R.drawable.ic_play_arrow_white_24dp);
        }catch (Exception e){
            Log.e("WordAdapter","Issue with Word data: "+e.toString());
        }

        return listItem;
    }
}
