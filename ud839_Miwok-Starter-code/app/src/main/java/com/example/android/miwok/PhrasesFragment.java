package com.example.android.miwok;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhrasesFragment extends Fragment {
    MediaPlayer mediaPlayer = null;
    AudioManager audioManager = null;
    AudioManager.OnAudioFocusChangeListener afChangeListener =
            new AudioManager.OnAudioFocusChangeListener(){
                @Override
                public void onAudioFocusChange(int focusChange) {
                    switch (focusChange){
                        case AudioManager.AUDIOFOCUS_GAIN:
                            mediaPlayer.start();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            releaseMediaPlayer();
                            break;
                        /** loss & loss transient both result in pause
                         * seekTo 0: whole pronunciation needs to be heard
                         */
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            mediaPlayer.pause();
                            mediaPlayer.seekTo(0);
                            break;
                        default:
                            Log.e(this.toString(),"Unaccounted audio state");
                            break;
                    }
                }
            };

    MediaPlayer.OnCompletionListener onCompleteListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };


    public PhrasesFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.word_list, container, false);
        String[] wordsArray = new String[]{
                "Where are you going?",
                "What is your name?",
                "My name is...",
                "How are you feeling?",
                "I’m feeling good.",
                "Are you coming?",
                "Yes, I’m coming.",
                "I’m coming.",
                "Let’s go.",
                "Come here."
        };
        String[] miwokArray = new String[]{
                "minto wuksus",
                "tinnә oyaase'nә",
                "oyaaset...",
                "michәksәs?",
                "kuchi achit",
                "әәnәs'aa?",
                "hәә’ әәnәm",
                "әәnәm",
                "yoowutis",
                "әnni'nem"
        };
        int[] audioArray = new int[]{
                R.raw.phrase_where_are_you_going,
                R.raw.phrase_what_is_your_name,
                R.raw.phrase_my_name_is,
                R.raw.phrase_how_are_you_feeling,
                R.raw.phrase_im_feeling_good,
                R.raw.phrase_are_you_coming,
                R.raw.phrase_yes_im_coming,
                R.raw.phrase_im_coming,
                R.raw.phrase_lets_go,
                R.raw.phrase_come_here
        };
        final ArrayList<Word> words = new ArrayList<Word>();
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

        for(int i=0;i<wordsArray.length;++i){
            words.add(i, new Word(wordsArray[i], miwokArray[i], audioArray[i]));
            Log.v("ColorsActivity"+this.getClass().toString(),
                    "Word at index "+i+": "+words.get(i));
        }

        WordAdapter adapter = new WordAdapter(getActivity(), words, R.color.category_phrases);
        ListView listView = (ListView) rootView.findViewById(R.id.list);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v("NumbersActivity", "Current word: " + words.get(position).toString());
                int audioId = words.get(position).getAudioResourceId();
                releaseMediaPlayer();

                int result = audioManager.requestAudioFocus(
                        afChangeListener,
                        AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    mediaPlayer = MediaPlayer.create(getActivity(), audioId);
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(onCompleteListener);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer(){
        if(mediaPlayer != null){
            mediaPlayer.release();
            mediaPlayer = null;
            audioManager.abandonAudioFocus(afChangeListener);
        }
    }
}
